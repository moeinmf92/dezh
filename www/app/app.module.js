(function () {

    'use strict';

    // Application modules
    var AppModules = [
        'ngRoute',
        'formValidation',
        'typeInputs',
        'tableList',
        'ngAnimate',
        'ngCookies',
        'ngMaterial',
        'ngMessages',
        'ngMeta',
        'ng',
        'ngResource',
        'ngStorage',
        'satellizer',
        'toastr',
        'ui.router',
        'checkImage',
        'ngFileUpload',
        'ngImgCrop',
        'dezhafzar.dialogs',
        'dezhafzar.config',
        'dezhafzar.views',
        'dezhafzar.datepicker',
        'dezhafzar.navigation',
        'dezhafzar.home',
        'dezhafzar.panel',
        'dezhafzar.default',
        'dezhafzar.login',
        'dezhafzar.logout',
        'dezhafzar.dashboard',
        'dezhafzar.users',
        'dezhafzar.logs'
    ];

    // Define application
    angular
        .module('dezhafzar', AppModules)
        .config(AppConfig)
        .run(AppRun);

    // Application injections
    AppConfig.$inject = [
        '$urlRouterProvider',
        '$locationProvider',
        '$urlProvider',
        '$authProvider',
        '$provide',
        'ngMetaProvider',
        '$qProvider',
        'toastrConfig'
    ];
    AppRun.$inject = [
        '$rootScope',
        '$mdMedia',
        '$mdSidenav',
        '$http',
        'ngMeta',
        '_api'
    ];

    // Application object
    function AppConfig($urlRouterProvider, $locationProvider, $urlProvider,$authProvider, $provide, ngMetaProvider, $qProvider, toastrConfig) {


        // $q config
        $qProvider.errorOnUnhandledRejections(true);

        // Meta config
        ngMetaProvider.useTitleSuffix(true);
        ngMetaProvider.setDefaultTitle('دژافزار');

        // Toastr config
        angular.extend(toastrConfig, {
            positionClass: 'toast-bottom-left',
            preventOpenDuplicates: true
        });

        // Delegate states
        $provide.decorator('$state', function ($delegate, $rootScope) {
            window.scrollTo(0, 0);
            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, options) {
                $delegate.nextState = {
                    name: toState.name,
                    params: toParams
                };
                $delegate.previousState = {
                    name: fromState.name,
                    params: fromParams
                };
            });
            return $delegate;
        });

        // Routing config
        $urlRouterProvider.otherwise('/');
        $locationProvider.html5Mode(true);

        // Authentication config
        var $url = $urlProvider.$get();
        $authProvider.httpInterceptor = false;
        $authProvider.loginUrl = $url.endpoint('admin-login').url;

    }

    function AppRun($rootScope, $mdMedia, $mdSidenav, $http, ngMeta, _api) {

        // Functions
        var Initialize = function () {
            $rootScope.$local = !!_api.local;
            $rootScope.$mdMedia = $mdMedia;
            $rootScope.$mdSidenav = $mdSidenav;
            $http.defaults.headers.common['Accept-Language'] = 'fa-IR;q=0.5,en-US;q=0.4';
            ngMeta.init();
        };

        // Variables
        $rootScope.$local = $rootScope.$mdMedia = $rootScope.$mdSidenav = {};

        // Initialize
        Initialize();

    }

})();