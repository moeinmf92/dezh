(function () {

    'use strict';

    // Define controller
    angular
        .module('dezhafzar.home')
        .controller('HomeController', HomeController);

    // Controller injections
    HomeController.$inject = [
        '$scope',
        '$rootScope',
        '$state',
        '$mdMedia',
        '$device'
    ];

    // Controller object
    function HomeController($scope, $rootScope, $state, $mdMedia, $device) {

        // Functions
        var Initialize = function () {
            $scope.$mdMedia = $mdMedia;
            SetStateName($state.current.name);
        };
        var SetStateName = function (arg) {
            $scope.state = arg.split('.')[1];
        };

        $scope.Device = function () {
            return $device();
        };


        // Event


        // Variables
        $scope.state = '';
        $scope.button = '';
        $scope.toggle = false;
        $scope.unrestricted = false;
        $scope.state = $scope.$mdMedia = {};

        // Initialize
        Initialize();

    }

})();