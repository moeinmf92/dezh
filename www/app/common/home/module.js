(function () {

    'use strict';

    // Define module
    angular
        .module('dezhafzar.home', [])
        .config(function ($stateProvider) {

            // State config
            $stateProvider
                .state('home', {
                    url: '/home',
                    abstract: true,
                    views: {
                        'layout': {
                            templateUrl: 'app/common/home/partial.html',
                            controller: 'HomeController'
                        }
                    },
                    resolve: {
                        navigation: function($rootScope){
                            $rootScope.navigation = false;
                        }
                    }
                });

        });

})();