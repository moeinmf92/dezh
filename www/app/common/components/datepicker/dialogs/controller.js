(function () {

    'use strict';

    // Define controller
    angular
        .module('dezhafzar.datepicker')
        .controller('datepickerController', datepickerController)

    // Controller injections
    datepickerController.$inject = [
        '$scope',
        '$q',
        '$crud',
        'toastr',
        '$timeout',
        'options',
        'scope',
        '$mdDialog'
    ];


    // Controller object
    function datepickerController($scope, $q, $crud, toastr, $timeout, options, scope, $mdDialog) {

        var Initialize = function () {
            console.log(scope)
            $scope.options = options;
            $timeout(function () {
                var pd = $("#datepicker .date").persianDatepicker({
                    "inline": true,
                    "format": options.format ? options.format : "LLLL",
                    "altField": '#dateAlt',
                    "viewMode": options.viewMode ? options.viewMode : "day",
                    "initialValue": options.initialValue !== undefined ? options.initialValue : true,
                    "minDate": options.minDate ? options.minDate : undefined,
                    "maxDate": options.maxDate ? options.maxDate : undefined,
                    "autoClose": true,
                    'position': 'auto',
                    "altFormat": "LLLL",
                    "onlyTimePicker": options.onlyTimePicker ? options.onlyTimePicker : false,
                    "onlySelectOnDate": false,
                    "calendarType": "persian",
                    "inputDelay": 800,
                    "observer": false,
                    "calendar": {
                        "persian": {
                            "locale": "fa",
                            "showHint": true,
                            "leapYearMode": "algorithmic"
                        },
                        "gregorian": {
                            "locale": "en",
                            "showHint": true
                        }
                    },
                    "navigator": {
                        "enabled": true,
                        "scroll": {
                            "enabled": true
                        },
                        "text": {
                            "btnNextText": "‹",
                            "btnPrevText": "›"
                        }
                    },
                    "toolbox": {
                        "enabled": true,
                        "calendarSwitch": {
                            "enabled": true,
                            "format": "MMMM"
                        },
                        "todayButton": {
                            "enabled": true,
                            "text": {
                                "fa": "امروز",
                                "en": "Today"
                            }
                        },
                        "submitButton": {
                            "enabled": true,
                            "text": {
                                "fa": "تایید",
                                "en": "Submit"
                            }
                        },
                        "text": {
                            "btnToday": "امروز"
                        }
                    },
                    "timePicker": {
                        "enabled": options.timePicker && options.timePicker.enabled ? true : false,
                        "step": 1,
                        "hour": {
                            "enabled": true,
                            "step": null
                        },
                        "minute": {
                            "enabled": true,
                            "step": null
                        },
                        "second": {
                            "enabled": false,
                            "step": null
                        },
                        "meridian": {
                            "enabled": false
                        }
                    },
                    "dayPicker": {
                        "enabled": true,
                        "titleFormat": "YYYY MMMM"
                    },
                    "monthPicker": {
                        "enabled": true,
                        "titleFormat": "YYYY"
                    },
                    "yearPicker": {
                        "enabled": true,
                        "titleFormat": "YYYY"
                    },
                    "responsive": true,
                    "formatter": function (unixDate) {
                        moment.locale('en');
                        var target = moment(unixDate);
                        var date = {
                            year: Number(target.format('YYYY')),
                            month: Number(target.format('MM')),
                            day: Number(target.format('DD')),
                            hour: Number(target.format('HH')),
                            minute: Number(target.format('mm')),
                            moment: target
                        };
                        unixDate = Number(moment(date.year + "-" + date.month + "-" + date.day , "YYYY-MM-DD").format('X') + '000') ;

                        selectedTime = unixDate;
                        var self = this;
                        var pdate = new persianDate(unixDate);
                        pdate.formatPersian = this.persianDigit;
                        return pdate.format(self.format);
                    },
                    "onSelect": function (unixDate) {
                    }
                });
                if (options.value)
                    pd.setDate(options.value);
                pd.show();
            }, 150)
        };
        $scope.reject = function () {
            $mdDialog.cancel();
        };
        $scope.reset = function () {
            $mdDialog.hide('');
        };
        $scope.submit = function () {
            // var timestamp = Number(selectedTime.toString().slice(0, -3));
            // console.log(timestamp)
            $mdDialog.hide(selectedTime);
        };


        // Variables
        var selectedTime = 0;

        // Initialize
        Initialize();
    }

})();

