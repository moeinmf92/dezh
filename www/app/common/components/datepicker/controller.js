/**
 * Created by Moein on 1/3/2018.
 */

(function () {

    'use strict';

    // Define directive
    angular
        .module('dezhafzar.datepicker')
        .directive('datepicker', datepicker);

    // Directive injections
    datepicker.$inject = [
        '$state',
        '$rootScope',
        '$authentication',
        '$timeout',
        '$mdDialog',
        '$q',
        '$crud',
        'toastr',
        '$url',
        '$auth',
        '$http'
    ];


    // Directive objects
    function datepicker($state, $rootScope, $authentication, $timeout, $mdDialog, $q, $crud, toastr, $url, $auth, $http) {
        return {
            restrict: 'E',
            require: 'ngModel',
            scope: {
                ngModel: '=ngModel',
                formName: '=formName',
                inputName: '@inputName',
                type: '@',
                config: "="
            },
            templateUrl: 'app/common/components/datepicker/partial.html',
            // bindToController: false,
            link: function (scope, element, attributes) {

                // Functions
                var Initialize = function () {
                    options = scope.config ? scope.config : {};
                    scope.required = attributes.required;

                    if (scope.ngModel)
                        scope.alt = moment(scope.ngModel).format("jYYYY/jM/jD ");
                };
                scope.openPopup = function () {
                    console.log('openpopup')

                    $mdDialog
                        .show({
                            parent: angular.element(document.body),
                            controller: 'datepickerController',
                            templateUrl: 'app/common/components/datepicker/dialogs/partial.html',
                            clickOutsideToClose: false,
                            multiple: true,
                            locals: {options: options, scope: scope}
                        })
                        .then(
                            function (timestamp) {
                                console.log(options)
                                if (options.type === 'to')
                                    timestamp += 86359999;
                                timestamp = Math.round(timestamp / 1000);
                                console.log('R R S ', timestamp);
                                scope.ngModel = timestamp;
                                if (timestamp)
                                    scope.alt = moment.unix(timestamp).format("jYYYY/jM/jD ");
                                else
                                    scope.alt = "";
                            },
                            function (response) {
                                console.log('R R F ', response)
                            }
                        )

                };

                // Watchers

                // Events
                scope.$on('datepicker:change', function (e, param) {
                    switch (scope.type) {
                        case 'from':
                            if (param.from) options = param.from;
                            break;
                        case 'to':
                            if (param.to) options = param.to;
                            break;
                    }
                    if (param.update !== undefined)
                        scope.alt = param.update;
                });
                // Variables
                var options = {};

                // Initialize
                Initialize();

            },

        }
    }

})();


