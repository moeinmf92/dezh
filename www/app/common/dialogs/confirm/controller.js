/**
 * Created by Moein on 4/20/2018.
 */
(function () {

    'use strict';

    // Define controller
    angular
        .module('dezhafzar.dialogs')
        .controller('ConfirmController', ConfirmController);

    // Controller injections
    ConfirmController.$inject = [
        '$scope',
        '$state',
        '$mdDialog',
        'data'
    ];

    // Controller object
    function ConfirmController($scope, $state,$mdDialog,data) {

        // Functions
        var Initialize = function(){
            console.log(data);
            $scope.data = data ? data : {};
        };

        $scope.reject = function () {
            $mdDialog.cancel();
        };
        $scope.accept = function () {
            $mdDialog.hide();
        };
        // Variables


        // Initialize
        Initialize()

    }

})();
