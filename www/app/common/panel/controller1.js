(function () {

    'use strict';

    // Define controller
    angular
        .module('dezhafzar.panel')
        .controller('PanelController', PanelController);

    // Controller injections
    PanelController.$inject = [
        '$scope',
        '$menu'
    ];

    // Controller object
    function PanelController($scope, $menu) {

        // Functions
        $scope.Collapse = function () {
            $scope.collapsed = !$scope.collapsed;
        };

        // Variables
        $scope.collapsed = false;
        $scope.menu = $menu;

    }

})();
