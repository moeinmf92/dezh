(function () {

    'use strict';

    // Define module
    angular
        .module('dezhafzar.panel', [])
        .config(function ($stateProvider) {
            $stateProvider
                .state('panel', {
                    url: '/panel',
                    abstract: true,
                    views: {
                        'layout': {
                            templateUrl: 'app/common/panel/partial.html',
                            controller: 'PanelController'
                        }
                    },
                    resolve: {
                        navigation: function($rootScope,$state){
                            $rootScope.navigation = true;
                        },
                        access: function ($q,$authentication,$state) {
                            var deferred = $q.defer();
                            if($authentication.isLogin())
                                deferred.resolve();
                            else
                                deferred.reject();
                            deferred
                                .promise
                                .catch(
                                    function(){
                                        $state.go('home.login', {nextState: $state.nextState});
                                    }
                                );
                            return deferred.promise;

                        }
                    }
                });
        });

})();