(function () {

    'use strict';

    // Define controller
    angular
        .module('dezhafzar.panel')
        .controller('PanelController', PanelController);

    // Controller injections
    PanelController.$inject = [
        '$scope',
        '$rootScope',
        '$menu',
        '$authentication',
        '$timeout'
    ];

    // Controller object
    function PanelController($scope, $rootScope, $menu, $authentication, $timeout) {

        // Functions
        var Initialize = function () {
            activeRole = $authentication.get('activeRole') ? $authentication.get('activeRole') : {};
            $timeout(function () {
                var node = $('.vAccordion--sidebar > .vpane > v-pane-content > div > .button.hover.active')[0];
                if (node) {
                    var active_parent_id = ((node.parentNode.parentNode.parentNode.id).match(/\d/g)).join("");
                    $scope.flipped[active_parent_id] = true;
                    $scope.expanded[active_parent_id] = true;
                }
            }, 1500)
        };
        $scope.getSVG = function(icon){
            return '/assets/img/icons/' + icon.text + '.svg';
        };

        $scope.expand = function () {
                $scope.animateMargin = true;
                $scope.collapsed = false;
                $timeout(function () {
                    $scope.animateMargin = false;
                    $(window).trigger('resize');
                }, 800);
        };
        $scope.close = function () {
            if (!$scope.lock){
                $scope.animateMargin = true;
                $scope.collapsed = true;
                $timeout(function () {
                    $scope.animateMargin = false;
                    $(window).trigger('resize');
                }, 800);
            }
        };

        $scope.flip_y = function (index) {
            $timeout(function () {
                $scope.expanded[index] ? $scope.flipped[index] = true : $scope.flipped[index] = false;
            }, 99);

        };
        $scope.hoverItem = function (event, item) {
            var element = event.target;
            var bodyRect = document.body.getBoundingClientRect(),
                elemRect = element.getBoundingClientRect(),
                offset = elemRect.top - bodyRect.top;

            // alert('Element is ' + offset + ' vertical pixels from <body>');
            var div = document.getElementById('link_menu');
            div.style.opacity = '1';
            div.style.top = 'inherit';
            div.style.marginTop = (offset - 73) + 'px';
            $scope.hover = true;
        };
        $scope.leaveItem = function (event, item) {
            var div = document.getElementById('link_menu');
            div.style.opacity = '0';
            div.style.marginTop = '0';

            $scope.hover = false;
        };
        var checkAccess = function (permission) {
            var access = false;
            if (activeRole.permissions) {
                for (var i = 0; i < activeRole.permissions.length; i++)
                    if (activeRole.permissions[i].name === permission) {
                        access = true;
                        break;
                    }
            }

            return access;
        }
        $scope.accessParent = function (menu) {
            if (!angular.equals(menu.permission, '') && menu.permission && activeRole.type !== 'مدیر') {
                if (checkAccess(menu.permission))
                    if (!menu.parent_id)
                        return true
            }
            else return !menu.parent_id;
        };
        $scope.accessChild = function (parent, child) {
            if (!angular.equals(child.permission, '') && child.permission && activeRole.type !== 'مدیر') {
                if (checkAccess(child.permission))
                    if (child.parent_id === parent.id)
                        return true
            }
            else return child.parent_id === parent.id;
        };
        $scope.CollapseDelay = function () {
            sidebar_hover = false;
            $timeout(function () {
                if (!sidebar_hover)
                    $scope.close()
            },500);
        };
        var delay = function (elem, callback) {
            var timeout = null;
            elem.onmouseover = function() {
                // Set timeout to be a timer which will invoke callback after 1s
                timeout = setTimeout(callback, 500);
                sidebar_hover = true;
            };

            elem.onmouseout = function() {
                // Clear any timers set to timeout
                clearTimeout(timeout);
                sidebar_hover = false;
            }
        };


        delay(document.getElementById('sidebar'), function() {
            $scope.expand();
        });

        // Events
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, options) {
            stable = false;
            $scope.animateMargin = false;
            $timeout(function () {
                stable = true;
                // $scope.Collapse();
            }, 1500)
        });
        // Variables
        var sidebar_hover = false;
        var stable = true;
        $scope.flipped = [];
        $scope.expanded = [];
        $scope.hover = false;
        $scope.lock = false;
        $scope.collapsed = true;
        $scope.menu = $menu;
        var activeRole;

        // Initialize
        Initialize()
    }

})();
