(function () {

    'use strict';

    // Define controller
    angular
        .module('dezhafzar.navigation')
        .controller('NavigationController', NavigationController);

    // Controller injections
    NavigationController.$inject = [
        '$rootScope',
        '$scope',
        '$state',
        '$url',
        '$q',
        '$mdDialog',
        '$mdSidenav',
        '$authentication',
        '$crud',
        '$menu',
        'toastr',
        '$interval'
    ];

    // Controller object
    function NavigationController($rootScope, $scope, $state, $url, $q, $mdDialog, $mdSidenav, $authentication, $crud, $menu, toastr, $interval) {

        // Functions
        var Initialize = function () {
            activeRole = $authentication.get('activeRole') ? $authentication.get('activeRole') : {};
            // $scope.profile = $authentication.get('info') ? $authentication.get('info') : {};
            $scope.menu = $menu;
            if ($scope.IsLogin())
                $scope.user = UserObject();
        };
        var UserObject = function () {
            var object = {
                activeRole: {},
                info: {},
                roles: [],
                permissions: []
            };
            for (var index in object)
                object[index] = $authentication.get(index);
            return object;
        };
        $scope.getSVG = function(icon){
            return '/assets/img/icons/' + icon.text + '.svg';
        };
        $scope.Navigate = function () {
            $mdSidenav('navigation').toggle();
        };
        $scope.IsLogin = function () {
            return $authentication.isLogin();
        };
        $scope.TrackingSubmit = function () {
            var deferred = $q.defer();
            // $scope.url = $location.protocol() + '://' + $location.host() + '/t/' + $scope.identifier;
            //  $scope.identifier = '';
            // .catch(
            //     function(){
            //         toastr.error('کد رهگیری وارد شده معتبر نمی باشد!');
            //     }
            // );
            deferred.resolve();
            return deferred.promise;
        };
       $scope.goToHome = function () {
         $state.go('home.default');
       };
        var checkAccess = function (permission) {
            var access = false;
            if (activeRole.permissions) {
                for (var i = 0; i < activeRole.permissions.length; i++)
                    if (activeRole.permissions[i].name === permission) {
                        access = true;
                        break;
                    }
            }

            return access;
        }
        $scope.accessParent = function (menu) {
            if (!angular.equals(menu.permission, '') && menu.permission && activeRole.type !== 'مدیر') {
                if (checkAccess(menu.permission))
                    if (!menu.parent_id)
                        return true
            }
            else return !menu.parent_id;
        };
        $scope.accessChild = function (parent, child) {
            if (!angular.equals(child.permission, '') && child.permission && activeRole.type !== 'مدیر') {
                if (checkAccess(child.permission))
                    if (child.parent_id === parent.id)
                        return true
            }
            else return child.parent_id === parent.id;
        };
        $scope.goTo = function (item) {
            if ($state.current.name !== 'panel.requests')
                $state.go('panel.requests', {msg_id: item.msg_id}, {reload: 'panel.requests'});
            else
                $rootScope.$broadcast('goTo',{msg_id : item.msg_id})
        };

        // Events
        $scope.$on('$stateChangeSuccess', function (event, data) {
            $mdSidenav('navigation').close();
        });

        // var tick = function() {
        //     $scope.clock = moment().format(" HH:mm:ss jYYYY/jM/jD");
        // };
        //
        // $interval(tick, 1000);

        // Variables
        $scope.flipped = [];
        $scope.identifier = '';
        $scope.menu = {};
        $scope.user = {};
        $scope.avatarDir = $url.endpoint('get_avatar').url;
        var activeRole;
        $rootScope.notifications = [
            // {
            //     "direction": "cc",
            //     "labels": [],
            //     "sent_date": 1532501322,
            //     "sender": {
            //         "username": "admin",
            //         "role_company": "ستاد",
            //         "firstname": "عباس",
            //         "title": "مهندس",
            //         "lastname": "خسروانی",
            //         "role_id": 24,
            //         "role_name": "مدیریت سامانه",
            //         "gender": "آقا",
            //         "role_department": "اداره مهندسی نقشه‌برداری"
            //     },
            //     "cc": [
            //         {
            //             "username": "jazayeri",
            //             "role_company": "ستاد",
            //             "firstname": "شهرام",
            //             "title": "دکتر",
            //             "lastname": "جزایری",
            //             "role_id": 1,
            //             "role_name": "رئیس اداره نقشه برداری",
            //             "gender": "آقا",
            //             "seen": false,
            //             "role_department": "اداره مهندسی نقشه‌برداری"
            //         }
            //     ],
            //     "text": "<p style=\"text-align: center;\"><b>TEST</b></p>",
            //     "req": {
            //         "prj_code": "1020",
            //         "prj_name": "companies",
            //         "applicant": {
            //             "username": "admin",
            //             "role_company": "ستاد",
            //             "firstname": "عباس",
            //             "title": "مهندس",
            //             "lastname": "خسروانی",
            //             "role_id": 24,
            //             "role_name": "مدیریت سامانه",
            //             "gender": "آقا",
            //             "role_department": "اداره مهندسی نقشه‌برداری"
            //         },
            //         "topic": "receiver salemnia",
            //         "date": 1532501322,
            //         "type": null,
            //         "id": 4
            //     },
            //     "msg_id": 4,
            //     "file_ids": [],
            //     "priority": "medium",
            //     "reply_date": null,
            //     "receiver": {
            //         "username": "salemnia",
            //         "role_company": "ستاد",
            //         "firstname": "سالم",
            //         "title": "مهندس",
            //         "lastname": "سالمنیا",
            //         "role_id": 2,
            //         "role_name": "رئیس واحد نقل و انتقا ت و امور اراضی",
            //         "gender": "آقا",
            //         "role_department": "اداره مهندسی نقشه‌برداری"
            //     },
            //     "seen": false,
            //     "reminder": null,
            //     "reply_text": null,
            //     "reply_action": null,
            //     "subject": "receiver salemnia"
            // }
        ];
        $rootScope.reminders = [
            // {
            //     "direction": "cc",
            //     "labels": [],
            //     "sent_date": 1532501322,
            //     "sender": {
            //         "username": "admin",
            //         "role_company": "ستاد",
            //         "firstname": "عباس",
            //         "title": "مهندس",
            //         "lastname": "خسروانی",
            //         "role_id": 24,
            //         "role_name": "مدیریت سامانه",
            //         "gender": "آقا",
            //         "role_department": "اداره مهندسی نقشه‌برداری"
            //     },
            //     "cc": [
            //         {
            //             "username": "jazayeri",
            //             "role_company": "ستاد",
            //             "firstname": "شهرام",
            //             "title": "دکتر",
            //             "lastname": "جزایری",
            //             "role_id": 1,
            //             "role_name": "رئیس اداره نقشه برداری",
            //             "gender": "آقا",
            //             "seen": false,
            //             "role_department": "اداره مهندسی نقشه‌برداری"
            //         }
            //     ],
            //     "text": "<p style=\"text-align: center;\"><b>TEST</b></p>",
            //     "req": {
            //         "prj_code": "1020",
            //         "prj_name": "companies",
            //         "applicant": {
            //             "username": "admin",
            //             "role_company": "ستاد",
            //             "firstname": "عباس",
            //             "title": "مهندس",
            //             "lastname": "خسروانی",
            //             "role_id": 24,
            //             "role_name": "مدیریت سامانه",
            //             "gender": "آقا",
            //             "role_department": "اداره مهندسی نقشه‌برداری"
            //         },
            //         "topic": "receiver salemnia",
            //         "date": 1532501322,
            //         "type": null,
            //         "id": 4
            //     },
            //     "msg_id": 4,
            //     "file_ids": [],
            //     "priority": "medium",
            //     "reply_date": null,
            //     "receiver": {
            //         "username": "salemnia",
            //         "role_company": "ستاد",
            //         "firstname": "سالم",
            //         "title": "مهندس",
            //         "lastname": "سالمنیا",
            //         "role_id": 2,
            //         "role_name": "رئیس واحد نقل و انتقا ت و امور اراضی",
            //         "gender": "آقا",
            //         "role_department": "اداره مهندسی نقشه‌برداری"
            //     },
            //     "seen": false,
            //     "reminder": null,
            //     "reply_text": null,
            //     "reply_action": null,
            //     "subject": "receiver salemnia"
            // }
        ];
        $scope.newReminder = true;
        $scope.newNotification = true;

        // Initialize
        Initialize();

    }

})();
