(function () {

    'use strict';

    // Define controller
    angular
        .module('dezhafzar.navigation')
        .controller('NavigationController', NavigationController);

    // Controller injections
    NavigationController.$inject = [
        '$scope',
        '$state',
        '$q',
        '$mdDialog',
        '$mdSidenav',
        '$crud',
        '$menu',
        'toastr',
        '$location'
    ];

    // Controller object
    function NavigationController($scope, $state, $q, $mdDialog, $mdSidenav, $crud, $menu, toastr,$location) {

        // Functions
        var Initialize = function(){
            $scope.menu = $menu;
        };
        $scope.PrivacyDialog = function () {
            $mdDialog.show({
                parent: angular.element(document.body),
                templateUrl: 'app/common/dialogs/privacy/partial.html',
                clickOutsideToClose: true
            });
        };
        $scope.Navigate = function(){
            $mdSidenav('navigation').toggle();
        };
        $scope.IsLogin = function () {
            // return $authentication.isLogin();
        };
        $scope.TrackingSubmit = function () {
            var deferred = $q.defer();
                $scope.url = $location.protocol() + '://' + $location.host() + '/t/' + $scope.identifier;
                 $scope.identifier = '';
                // .catch(
                //     function(){
                //         toastr.error('کد رهگیری وارد شده معتبر نمی باشد!');
                //     }
                // );
            deferred.resolve();
            return deferred.promise;
        };
        $scope.TrackingReset = function(){

        };
        $scope.TrackingInvalid = function(){
            toastr.error('کد رهگیری وارد شده معتبر نمی باشد!');
        };
        $scope.TrackingPristine = function () {
            toastr.error('لطفا کد رهگیری خود را وارد نمایید!');
        };

        // Events
        $scope.$on('$stateChangeSuccess', function (event, data) {
            $mdSidenav('navigation').close();
        });

        // Variables
        $scope.identifier = '';
        $scope.menu = {};

        // Initialize
        Initialize();

    }

})();
