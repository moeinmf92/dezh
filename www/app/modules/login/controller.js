(function () {

    'use strict';

    // Define controller
    angular
        .module('dezhafzar.login')
        .controller('LoginController', LoginController);

    // Controller injections
    LoginController.$inject = [
        '$scope',
        '$authentication',
        '$state',
        '$q',
        '$crud',
        '$timeout',
        'toastr'
    ];

    // Controller object
    function LoginController($scope,$authentication,$state, $q, $crud,$timeout,toastr) {

        // Functions
        var Initialize = function () {
            $timeout(function () {
                $scope.loginForm.$setDirty();
            }, 99);
        };


        $scope.return = function(){
            $state.go('home.default');
        };


        $scope.loginSubmit = function(){


                        // $state.go('panel.dashboard');


            var deferred = $q.defer();
            console.log($scope.login);
            $authentication
                .login($scope.login)
                .then(
                    function (response) {
                        $state.go('panel.dashboard');
                        deferred.resolve(response);

                    },
                    function (response) {
                        toastr.error('نام کاربری یا گذرواژه صحیح نمی باشد.');
                        deferred.reject(response);
                        console.log('LoginController.LoginSubmit()');
                    }
                );
            return deferred.promise;
        };

        // Watchers

        // Variables
        $scope.login = {
            username : '',
            password : ''
        };

        // Initialize
        Initialize()

    }

})();
