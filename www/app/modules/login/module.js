(function () {

    'use strict';

    // Define module
    angular
        .module('dezhafzar.login', [])
        .config(function ($menuProvider, $stateProvider) {

            // Menu config

            // State config
            $stateProvider
                .state('home.login', {
                    url: '^/login',
                    views: {
                        'nested@home': {
                            templateUrl: 'app/modules/login/partial.html',
                            controller: 'LoginController'
                        }
                    },
                    data: {
                        meta: {
                            'titleSuffix': ' | ورود'
                        }
                    }
                });

        });

})();