(function() {

    'use strict';

    // Define module
    angular
        .module('dezhafzar.logs', [])
        .config(function($menuProvider, $stateProvider) {

            // Menu config
            $menuProvider.set('user', {
                id: '3',
                sref: 'panel.logs',
                text: 'رویداد های سیستم',
                icon: {
                    type: 'material',
                    text: 'reorder'
                }
            });

            // State config
            $stateProvider
                .state('panel.logs', {
                    url: '^/logs',
                    views: {
                        'module': {
                            templateUrl: 'app/modules/logs/partial.html',
                            controller: 'LogsController'
                        }
                    },
                    data: {
                        meta: {
                            'titleSuffix': ' | رویداد های سیستمی'
                        }
                    }
                });

        });

})();