(function () {

    'use strict';

    // Define controller
    angular
        .module('dezhafzar.logs')
        .controller('LogsController', LogsController);

    // Controller injections
    LogsController.$inject = [
        '$scope',
        '$rootScope',
        '$mdDialog',
        '$q',
        '$timeout',
        '$crud',
        'toastr'
    ];

    // Controller object
    function LogsController($scope, $rootScope, $mdDialog, $q, $timeout, $crud, toastr) {

        // Functions
        var Initialize = function () {

        };


        $scope.GetLogsList = function (query) {
            var deferred = $q.defer();
            if (!angular.equals($scope.filterModel, {})) {
                var params = angular.copy($scope.logsList.params);
                var data = angular.merge(params, $scope.filterModel);
                data = angular.merge(data, {'quick': $scope.quickSearch});
                $crud('logs')
                    .url("list")
                    .type('form')
                    .post(data)
                    .then(
                        function (response) {
                            $scope.items = response.msg.logs ? response.msg.logs : [];
                            moment.locale('fa');
                            moment.loadPersian({usePersianDigits: true});
                            $scope.items.map(function (item) {
                                item.r_created_date_fa = item.created_date ? moment(moment.unix(item.created_date).format("YYYYMMDD HHmmss")).fromNow() : '';
                                item.created_date_fa = moment.unix(item.created_date).format("HH:mm:s jYYYY/jM/jD ");

                                // item.created_date_fa =
                            });
                            deferred.resolve({
                                total: response.msg.count,
                                items: $scope.items
                            });
                        },
                        function (response) {
                            console.log('LogsController.GetLogsList()');
                            deferred.reject();
                        }
                    );
            }
            else
                deferred.resolve({
                    total: 0,
                    items: []
                });
            return deferred.promise;
        };


        // Events

        // Watchers
        $scope.$watch('filterModel', function (newVal, oldVal) {
            console.log(newVal, oldVal);
            if (!angular.equals(newVal, {})) {
                    $scope.logsList.getList();
            }

        }, true);
        $scope.$watch('filterModel.created_date_from', function (newVal, oldVal) {
            $scope.fromDatepickerConfig.value = newVal ? Number(newVal + '000') : null;
            $scope.toDatepickerConfig.minDate = newVal ? Number(newVal + '000') : null;
            $scope.toDatepickerConfig.maxDate = $scope.filterModel.created_date_to ? $scope.filterModel.created_date_to : Date.now();
            $rootScope.$broadcast('datepicker:change', {to: $scope.toDatepickerConfig});
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        });
        $scope.$watch('filterModel.created_date_to', function (newVal, oldVal) {
            $scope.toDatepickerConfig.value = newVal ? Number(newVal + '000') : Date.now();
            $scope.fromDatepickerConfig.maxDate = newVal ? Number(newVal + '000') : Date.now();
            $rootScope.$broadcast('datepicker:change', {from: $scope.fromDatepickerConfig});
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        });
        $scope.$watch('quickSearch', function (newVal, oldVal) {

            if (newVal !== oldVal && (oldVal !== '' || newVal !== '')) {
                $scope.logsList.getList();
            }
        });

        // Variables
        $scope.items = [];
        $scope.selected = [];


        $scope.quickSearch = "";
        $scope.filterModel = {};
        $scope.filterConfig = [
            {
                key: 'username',
                name: 'نام کاربری',
                value: '',
                type: 'string'
            },
            {
                key: 'status_code',
                name: 'کد وضعیت',
                value: '',
                type: 'string'
            },
            {
                key: 'created_date',
                name: 'تاریخ ایجاد',
                value: '',
                type: 'date'
            },

        ];
        $scope.fromDatepickerConfig = {
            "type" : "from",
            "format": "LLLL",
            "viewMode": "day",
            "minDate": null,
            "maxDate": Date.now(),
            "onlyTimePicker": false,
            "initialValue": false,
            "title": 'انتخاب تاریخ شروع'
        };
        $scope.toDatepickerConfig = {
            "type" : "to",
            "format": "LLLL",
            "viewMode": "day",
            "minDate": null,
            "maxDate": Date.now(),
            "onlyTimePicker": false,
            "initialValue": false,
            "title": 'انتخاب تاریخ پایان'
        };
        $scope.filterEnable = true;


        // Initialize
        Initialize()

    }

})();
