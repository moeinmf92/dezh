(function () {

        'use strict';

        // Define controller
        angular
            .module('dezhafzar.users')
            .controller('UpdateUserController', UpdateUserController)
            .controller('ReferencesController', ReferencesController)
            .controller('EvaluationsController', EvaluationsController)
            .controller('DialogController', DialogController)


        // Controller injections
        UpdateUserController.$inject = [
            '$scope',
            '$mdDialog',
            '$url',
            '$auth',
            '$crud',
            '$q',
            'toastr',
            'Upload',
            'data'
        ];
        ReferencesController.$inject = [
            '$scope',
            '$rootScope',
            '$mdDialog',
            '$q',
            '$timeout',
            '$crud',
            'toastr',
            'data'
        ];
        EvaluationsController.$inject = [
            '$scope',
            '$rootScope',
            '$mdDialog',
            '$q',
            '$timeout',
            '$crud',
            'toastr',
            'data'
        ];
        DialogController.$inject = [
            '$scope',
            '$rootScope',
            '$mdDialog',
            '$q',
            '$timeout',
            '$crud',
            'toastr',
            'data'
        ];


        // Controller object
        function UpdateUserController($scope, $mdDialog, $url, $auth, $crud, $q, toastr,Upload, data) {
            var Initialize = function () {
                console.log("retrieve params in dialog", data);
                $scope.user = userObject();

            };

            var userObject = function () {
                if (!angular.equals(data, {})) {
                    $scope.mode = 'edit';
                    return data;
                }
                else
                    return {
                        username: "",
                        description: "",
                        avatar : "",
                        fullname: "",
                        type: "user"
                    }
            };
            $scope.reject = function () {
                $mdDialog.cancel();
            };
            $scope.UserSubmit = function () {
                console.log($scope.user)
                var deferred = $q.defer();
                if ($scope.mode === 'edit')
                    editUser().then(function () {
                        deferred.resolve($mdDialog.hide())
                    }, function () {
                        deferred.reject();
                    });
                else
                    addUser().then(function () {
                        deferred.resolve($mdDialog.hide());
                    }, function () {
                        deferred.reject();
                    });
                return deferred.promise;
                // $mdDialog.hide();
            };
            var addUser = function () {
                var deferred = $q.defer();
                $crud('users')
                    .url("add")
                    .type("form")
                    .post($scope.user)
                    .then(
                        function (response) {
                            $mdDialog.hide();
                            toastr.success('کاربر با موفقیت ثبت گردید');
                            deferred.resolve();
                        },
                        function (response) {
                            toastr.error('متاسفانه خطایی در ثبت اطلاعات وجود دارد !');
                            console.log('UpdateUserController.addUser()');
                            deferred.reject();
                        }
                    );
                return deferred.promise;
            };
            var editUser = function () {
                var deferred = $q.defer();
                $crud('users')
                    .url("update")
                    .type("form")
                    .post($scope.user)
                    .then(
                        function (response) {
                            // $mdDialog.hide();
                            toastr.success('کاربر با موفقیت ویرایش گردید');
                            deferred.resolve();
                        },
                        function (response) {
                            console.log('UpdateUserController.editUser()');
                            deferred.reject();
                        }
                    );
                return deferred.promise;
            };


            $scope.UploadSubmit = function () {
                console.log("`UploadSubmit" , Upload.dataUrltoBlob($scope.imageCrop, $scope.image.name));
                var deferred = $q.defer();
                // avatar = angular.copy(Upload.dataUrltoBlob($scope.imageCrop, $scope.image.name));
                $scope.avatar_url = angular.copy($scope.imageCrop);
                $scope.user.avatar = angular.copy(Upload.dataUrltoBlob($scope.imageCrop, $scope.image.name));
                $scope.upload = false;
                $scope.image = undefined;
                deferred.resolve();
                return deferred.promise;
            };

            $scope.ImageSelect = function (event) {
                if (typeof $scope.image != 'undefined')
                    event.stopPropagation();
            };

            // Variables
            $scope.roles = [];
            $scope.user = {};
            $scope.mode = 'create';
            $scope.upload = false;
            var avatar;
            $scope.avatar_url = '';
            $scope.avatarDir = $url.endpoint('get_avatar').url;
            $scope.today = new Date();

            // Initialize
            Initialize();
        }

        function ReferencesController($scope, $rootScope, $mdDialog, $q, $timeout, $crud, toastr, data) {

            // Functions
            var Initialize = function () {
                $scope.data = data;
            };
            $scope.GetRefsList = function (query) {
                var deferred = $q.defer();
                if (!angular.equals($scope.refFilterModel, {})) {
                    var params = angular.copy($scope.refsList.params);
                    var data = angular.merge(params, $scope.refFilterModel);
                    data = angular.merge(data, {'quick': $scope.quickSearch});
                    data.username = $scope.data.username;
                    $crud('refs')
                        .url("list")
                        .type('form')
                        .post(data)
                        .then(
                            function (response) {
                                moment.locale('fa');
                                moment.loadPersian({usePersianDigits: true});
                                $scope.items = response.msg.references ? response.msg.references : [];
                                $scope.items.map(function (item) {
                                    item.r_created_date_fa = item.created_date ? moment(moment.unix(item.created_date).format("YYYYMMDD HHmmss")).fromNow() : '';
                                    item.created_date_fa = moment.unix(item.created_date).format("HH:mm:s jYYYY/jM/jD ");
                                });
                                deferred.resolve({
                                    total: response.msg.count,
                                    items: $scope.items
                                });
                            },
                            function (response) {
                                console.log('UsersController.GetRefsList()');
                                deferred.reject(response);
                            }
                        );
                } else
                    deferred.resolve({
                        total: 0,
                        items: []
                    });
                return deferred.promise;
            }
            ;
            $scope.delete = function (ref) {
                console.log("deleteUser", ref);
                var deferred = $q.defer();
                $mdDialog
                    .show({
                        parent: angular.element(document.body),
                        controller: 'ConfirmController',
                        templateUrl: 'app/common/dialogs/confirm/partial.html',
                        clickOutsideToClose: false,
                        multiple: true,
                        locals: {data: {description: 'آیا از حذف این مرجع اطمینان دارید ؟ ', hint: ''}}
                    })
                    .then(
                        function (response) {
                            $crud('refs')
                                .url('remove')
                                .get({ids: ref.id})
                                .then(
                                    function (response) {
                                        toastr.success("مرجع با موفقیت حذف گردید.");
                                        $scope.refsList.getList();
                                        deferred.resolve();
                                    },
                                    function (response) {
                                        toastr.error('متاسفانه خظایی در حذف مرجع به وجود آمده است.');
                                        console.log('ReferencesController.delete()');
                                        deferred.reject();
                                    });
                        }
                    );
                return deferred.promise;
            };
            $scope.download = function (ref) {
                console.log("download", ref);
                var deferred = $q.defer();
                $crud('ref-download')
                    .type('form')
                    .get({id: ref.id})
                    .then(
                        function (response) {
                            var filename = new Date().toISOString() + '.wav';
                            // The actual download
                            var blob = new Blob([response]);
                            var link = document.createElement('a');
                            link.href = window.URL.createObjectURL(blob);
                            link.download = filename;
                            document.body.appendChild(link);
                            link.click();
                            document.body.removeChild(link);
                            deferred.resolve();
                        },
                        function (response) {
                            toastr.error('متاسفانه خظایی در دانلود صوت مرجع به وجود آمده است.');
                            console.log('ReferencesController.download()');
                            deferred.reject();
                        });

                return deferred.promise;

            };
            $scope.enroll = function (ref) {
                console.log("enroll", ref);
                var deferred = $q.defer();
                $mdDialog
                    .show({
                        parent: angular.element(document.body),
                        controller: 'DialogController',
                        templateUrl: 'app/modules/users/dialogs/dialogs.html',
                        clickOutsideToClose: false,
                        multiple: true,
                        locals: {data: $scope.data}
                    })
                    .then(
                        function (response) {
                            $scope.refsList.getList();
                        }
                    );
                return deferred.promise;
            };
            $scope.reject = function () {
                $mdDialog.cancel();
            };


            // Events

            // Watchers
            $scope.$watch('refFilterModel', function (newVal, oldVal) {
                console.log(newVal, oldVal);
                if (!angular.equals(newVal, {})) {
                    $scope.refsList.getList();
                }

            }, true);
            $scope.$watch('refFilterModel.created_date_from', function (newVal, oldVal) {
                $scope.fromDatepickerConfig.value = newVal ? Number(newVal + '000') : null;
                $scope.toDatepickerConfig.minDate = newVal ? Number(newVal + '000') : null;
                $scope.toDatepickerConfig.maxDate = $scope.refFilterModel.created_date_to ? $scope.refFilterModel.created_date_to : Date.now();
                $rootScope.$broadcast('datepicker:change', {to: $scope.toDatepickerConfig});
                if (!$scope.$$phase && !$rootScope.$$phase) {
                    $scope.$apply();
                }
            });
            $scope.$watch('refFilterModel.created_date_to', function (newVal, oldVal) {
                $scope.toDatepickerConfig.value = newVal ? Number(newVal + '000') : Date.now();
                $scope.fromDatepickerConfig.maxDate = newVal ? Number(newVal + '000') : Date.now();
                $rootScope.$broadcast('datepicker:change', {from: $scope.fromDatepickerConfig});
                if (!$scope.$$phase && !$rootScope.$$phase) {
                    $scope.$apply();
                }
            });

            // Variables
            $scope.items = [];
            $scope.selected = [];


            $scope.refFilterModel = {};
            $scope.refFilterConfig = [
                // {
                //     key: 'fullname',
                //     name: 'نام کامل',
                //     value: '',
                //     type: 'string'
                // },
                {
                    key: 'status',
                    name: 'وضعیت',
                    value: '',
                    type: 'string'
                },
                {
                    key: 'created_date',
                    name: 'تاریخ ایجاد',
                    value: '',
                    type: 'date'
                },

            ];
            $scope.fromDatepickerConfig = {
                "format": "LLLL",
                "viewMode": "day",
                "minDate": null,
                "maxDate": Date.now(),
                "onlyTimePicker": false,
                "initialValue": false,
                "title": 'انتخاب تاریخ شروع'
            };
            $scope.toDatepickerConfig = {
                "format": "LLLL",
                "viewMode": "day",
                "minDate": null,
                "maxDate": Date.now(),
                "onlyTimePicker": false,
                "initialValue": false,
                "title": 'انتخاب تاریخ پایان'
            };
            $scope.filterEnable = true;

            // Initialize
            Initialize()

        }


        function EvaluationsController($scope, $rootScope, $mdDialog, $q, $timeout, $crud, toastr, data) {

            // Functions
            var Initialize = function () {
                $scope.data = data;
            };

            $scope.GetEvalsList = function (query) {
                var deferred = $q.defer();
                if (!angular.equals($scope.evalsFilterModel, {})) {
                    var params = angular.copy($scope.evalsList.params);
                    var data = angular.merge(params, $scope.evalsFilterModel);
                    // data = angular.merge(data, {'quick': $scope.quickSearch});
                    data.username = $scope.data.username;
                    $crud('evals')
                        .url("list")
                        .type('form')
                        .post(data)
                        .then(
                            function (response) {
                                moment.locale('fa');
                                moment.loadPersian({usePersianDigits: true});
                                $scope.items = response.msg.evaluations ? response.msg.evaluations : [];
                                $scope.items.map(function (item) {
                                    item.r_created_date_fa = item.created_date ? moment(moment.unix(item.created_date).format("YYYYMMDD HHmmss")).fromNow() : '';
                                    item.created_date_fa = moment.unix(item.created_date).format("HH:mm:s jYYYY/jM/jD ");
                                });
                                deferred.resolve({
                                    total: response.msg.count,
                                    items: $scope.items
                                });
                            },
                            function (response) {
                                console.log('UsersController.GetRefsList()');
                                deferred.reject(response);
                            }
                        );
                }
                else
                    deferred.resolve({
                        total: 0,
                        items: []
                    });
                return deferred.promise;
            };


            $scope.delete = function (evaluation) {
                console.log("deleteUser", evaluation);
                var deferred = $q.defer();
                $mdDialog
                    .show({
                        parent: angular.element(document.body),
                        controller: 'ConfirmController',
                        templateUrl: 'app/common/dialogs/confirm/partial.html',
                        multiple:true,
                        clickOutsideToClose: false,
                        locals: {data: {description: 'آیا از حذف این ارزیابی اطمینان دارید ؟ ', hint: ''}}
                    })
                    .then(
                        function (response) {
                            $crud('evals')
                                .url('remove')
                                .get({ids: evaluation.id})
                                .then(
                                    function (response) {
                                        toastr.success("ارزیابی با موفقیت حذف گردید.");
                                        $scope.evalsList.getList();
                                        deferred.resolve();
                                    },
                                    function (response) {
                                        toastr.error('متاسفانه خظایی در حذف ارزیابی به وجود آمده است.');
                                        console.log('EvaluationsController.delete()');
                                        deferred.reject();
                                    });
                        }
                    );
                return deferred.promise;
            };


            $scope.download = function (evaluation) {
                console.log("download", evaluation);
                var deferred = $q.defer();
                $crud('eval-download')
                    .type('form')
                    .get({id: evaluation.id})
                    .then(
                        function (response) {
                            var filename = new Date().toISOString() + '.wav';
                            // The actual download
                            var blob = new Blob([response]);
                            var link = document.createElement('a');
                            link.href = window.URL.createObjectURL(blob);
                            link.download = filename;
                            document.body.appendChild(link);
                            link.click();
                            document.body.removeChild(link);
                            deferred.resolve();
                        },
                        function (response) {
                            toastr.error('متاسفانه خظایی در دانلود صوت ارزیابی به وجود آمده است.');
                            console.log('EvaluationsController.download()');
                            deferred.reject();
                        });

                return deferred.promise;

            };



            $scope.reject = function () {
                $mdDialog.cancel();
            };


            // Events

            // Watchers
            $scope.$watch('evalsFilterModel', function (newVal, oldVal) {
                console.log(newVal, oldVal);
                if (!angular.equals(newVal, {})) {
                    $scope.evalsList.getList();
                }

            }, true);

            $scope.$watch('evalsFilterModel.created_date_from', function (newVal, oldVal) {
                $scope.fromDatepickerConfig.value = newVal ? Number(newVal + '000') : null;
                $scope.toDatepickerConfig.minDate = newVal ? Number(newVal + '000') : null;
                $scope.toDatepickerConfig.maxDate = $scope.evalsFilterModel.created_date_to ? $scope.evalsFilterModel.created_date_to : Date.now();
                $rootScope.$broadcast('datepicker:change', {to: $scope.toDatepickerConfig});
                if (!$scope.$$phase && !$rootScope.$$phase) {
                    $scope.$apply();
                }
            });
            $scope.$watch('evalsFilterModel.created_date_to', function (newVal, oldVal) {
                $scope.toDatepickerConfig.value = newVal ? Number(newVal + '000') : Date.now();
                $scope.fromDatepickerConfig.maxDate = newVal ? Number(newVal + '000') : Date.now();
                $rootScope.$broadcast('datepicker:change', {from: $scope.fromDatepickerConfig});
                if (!$scope.$$phase && !$rootScope.$$phase) {
                    $scope.$apply();
                }
            });


            // Variables



            $scope.evalsFilterModel = {};
            $scope.evalsFilterConfig = [
                {
                    key: 'status',
                    name: 'وضعیت',
                    value: '',
                    type: 'string'
                },
                {
                    key: 'created_date',
                    name: 'تاریخ ایجاد',
                    value: '',
                    type: 'date'
                },

            ];
            $scope.fromDatepickerConfig = {
                "format": "LLLL",
                "viewMode": "day",
                "minDate": null,
                "maxDate": Date.now(),
                "onlyTimePicker": false,
                "initialValue": false,
                "title": 'انتخاب تاریخ شروع'
            };
            $scope.toDatepickerConfig = {
                "format": "LLLL",
                "viewMode": "day",
                "minDate": null,
                "maxDate": Date.now(),
                "onlyTimePicker": false,
                "initialValue": false,
                "title": 'انتخاب تاریخ پایان'
            };
            $scope.filterEnable = true;


            // Initialize
            Initialize()

        }

        function DialogController($scope, $rootScope, $mdDialog, $q, $timeout, $crud, toastr, data) {

            // Functions
            var Initialize = function () {
                $scope.data = data;
                getPrompt();
                RecorderInitialize();
            };


            var getPrompt = function () {
                /**
                 * GET PROMPT TEXTS
                 */
                var deferred = $q.defer();
                $crud('prompt')
                    .url('ref')
                    .get()
                    .then(
                        function (response) {
                            $scope.data.prompt = response.msg.text[0];
                            deferred.resolve();
                        },
                        function (response) {
                            toastr.error('متاسفانه خظایی در دریافت متن به وجود آمده است.');
                            console.log('DialogController.getPrompt()');
                            deferred.reject();
                        }
                    );
                return deferred.promise;
            };


            /**
             * Patch the APIs for every browser that supports them and check
             * if getUserMedia is supported on the browser.
             *
             */
            function RecorderInitialize() {
                try {


                    navigator.getUserMedia = (
                        navigator.getUserMedia ||
                        navigator.webkitGetUserMedia ||
                        navigator.mozGetUserMedia ||
                        navigator.msGetUserMedia
                    );

                    if (typeof navigator.mediaDevices.getUserMedia === 'undefined') {
                        navigator.getUserMedia({
                            audio: true
                        }, function () {


                            // Monkeypatch for AudioContext, getUserMedia and URL
                            window.AudioContext = window.AudioContext || window.webkitAudioContext;
                            navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia;
                            window.URL = window.URL || window.webkitURL;

                            // Store the instance of AudioContext globally

                            audio_context = new AudioContext;
                            console.log('Audio context is ready !');
                            console.log('navigator.getUserMedia ' + (navigator.getUserMedia ? 'available.' : 'not present!'));


                        }, function () {
                            alert('No web audio support in this browser!');
                        });
                    } else {
                        navigator.mediaDevices.getUserMedia({
                            audio: true
                        }).then(function () {

                            // Monkeypatch for AudioContext, getUserMedia and URL
                            window.AudioContext = window.AudioContext || window.webkitAudioContext;
                            navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia;
                            window.URL = window.URL || window.webkitURL;

                            // Store the instance of AudioContext globally

                            audio_context = new AudioContext;
                            console.log('Audio context is ready !');
                            console.log('navigator.getUserMedia ' + (navigator.getUserMedia ? 'available.' : 'not present!'));


                        }).catch(function () {
                            alert('No web audio support in this browser!');
                        });
                    }


                } catch (e) {
                    alert('No web audio support in this browser!');
                }
            }

            /**
             * Starts the recording process by requesting the access to the microphone.
             * Then, if granted proceed to initialize the library and store the stream.
             *
             * It only stops when the method stopRecording is triggered.
             */
            function startRecording() {
                // Access the Microphone using the navigator.getUserMedia method to obtain a stream

                navigator.getUserMedia({audio: true}, function (stream) {


                    // Expose the stream to be accessible globally
                    audio_stream = stream;
                    // Create the MediaStreamSource for the Recorder library
                    var input = audio_context.createMediaStreamSource(stream);
                    console.log('Media stream succesfully created');


                    // Initialize the Recorder Library
                    recorder = new Recorder(input, {numChannels: 1});
                    console.log('Recorder initialised');

                    // Start recording !
                    recorder && recorder.record();
                    console.log('Recording...');

                    // Disable Record button and enable stop button !
                    // document.getElementById("start-btn").disabled = true;
                    // document.getElementById("stop-btn").disabled = false;
                }, function (e) {
                    console.error('No live audio input: ' + e);
                });
            }

            /**
             * Stops the recording process. The method expects a callback as first
             * argument (function) executed once the AudioBlob is generated and it
             * receives the same Blob as first argument. The second argument is
             * optional and specifies the format to export the blob either wav or mp3
             */
            function stopRecording(callback, AudioFormat) {

                // Stop the recorder instance
                // audio_context.close();
                recorder && recorder.stop();

                console.log('Stopped recording.');

                if (audio_stream)
                // Stop the getUserMedia Audio Stream !
                    audio_stream.getAudioTracks()[0].stop();

                // Disable Stop button and enable Record button !
                // document.getElementById("start-btn").disabled = false;
                // document.getElementById("stop-btn").disabled = true;

                // Use the Recorder Library to export the recorder Audio as a .wav file
                // The callback providen in the stop recording method receives the blob
                if (typeof(callback) === "function") {
                    /**
                     * Export the AudioBLOB using the exportWAV method.
                     * Note that this method exports too with mp3 if
                     * you provide the second argument of the function
                     */
                    recorder && recorder.exportWAV(function (blob) {
                        callback(blob);
                        // create WAV download link using audio data blob
                        // createDownloadLink();
                        // Clear the Recorder to start again !
                        recorder.clear();
                    }, 16000, (AudioFormat || "audio/wav"));
                }
            }

            //
            // $scope.record = function () {
            //     $scope.state.status = 'record';
            // };
            $scope.stopRecord = function () {
                $scope.state.status = 'stop';
                stopTimer();
                if (angular.equals(recordBLOB, {}))
                    stopRecording(function (AudioBLOB) {
                        recordBLOB = AudioBLOB;

                        console.log("stopRecord");
                        var deferred = $q.defer();
                        $mdDialog
                            .show({
                                parent: angular.element(document.body),
                                controller: 'ConfirmController',
                                templateUrl: 'app/common/dialogs/confirm/partial.html',
                                clickOutsideToClose: false,
                                multiple: true,
                                locals: {data: {description: ' تمایل به افزودن این صوت دارید ؟ ', hint: ''}}
                            })
                            .then(
                                function (response) {
                                    var deferred = $q.defer();
                                    $crud('voice')
                                        .url("enroll")
                                        .type('form')
                                        .post({
                                            username: $scope.data.username,
                                            prompt: $scope.data.prompt ? $scope.data.prompt : '',
                                            audio: recordBLOB
                                        })
                                        .then(
                                            function (response) {
                                                toastr.success('با موفقیت ثبت گردید.');
                                                $mdDialog.hide();
                                                deferred.resolve();
                                            },
                                            function (response) {
                                                toastr.error('خطایی در ارسال صوت به وجود آمده است.');
                                                console.log('DialogController.stopRecord()');
                                                deferred.reject();
                                            }
                                        );
                                    return deferred.promise;

                                }
                            );
                        return deferred.promise;


                    }, null);

                else {


                    console.log("stopRecord");
                    var deferred = $q.defer();
                    $mdDialog
                        .show({
                            parent: angular.element(document.body),
                            controller: 'ConfirmController',
                            templateUrl: 'app/common/dialogs/confirm/partial.html',
                            clickOutsideToClose: false,
                            multiple: true,
                            locals: {data: {description: ' تمایل به افزودن این صوت دارید ؟ ', hint: ''}}
                        })
                        .then(
                            function (response) {
                                var deferred = $q.defer();
                                $crud('voice')
                                    .url("enroll")
                                    .type('form')
                                    .post({
                                        username: $scope.data.username,
                                        prompt: $scope.data.prompt ? $scope.data.prompt : '',
                                        audio: recordBLOB
                                    })
                                    .then(
                                        function (response) {
                                            toastr.success('با موفقیت ثبت گردید.');
                                            $mdDialog.hide();
                                            deferred.resolve();
                                        },
                                        function (response) {
                                            toastr.error('خطایی در ارسال صوت به وجود آمده است.');
                                            console.log('DialogController.stopRecord()');
                                            deferred.reject();
                                        }
                                    );
                                return deferred.promise;

                            }
                        );
                    return deferred.promise;


                }



            };

            $scope.resetRecord = function () {
                $scope.state.mode = "record";
                $scope.state.status = "record";
                recordBLOB = {};
                resetTimer();
                startTimer();
                startRecording();

            };

            $scope.record = function () {

                $scope.state.mode = "record";
                $scope.state.status = "record";
                minutes = document.querySelector('.minutes');
                seconds = document.querySelector('.seconds');
                recordBLOB = {};
                startTimer();
                startRecording();


            };

            function startTimer() {
                if (isRunning) return;

                isRunning = true;
                interval = setInterval(incrementTimer, 1000);
            }

            function stopTimer() {
                if (!isRunning) return;

                isRunning = false;
                clearInterval(interval);
            }

            function resetTimer() {
                timerTime = 0;
                minutes.innerText = '00';
                seconds.innerText = '00';
            }

            function incrementTimer() {
                timerTime++;

                var numberOfMinutes = Math.floor(timerTime / 60);
                var numberOfSeconds = timerTime % 60;

                minutes.innerText = pad(numberOfMinutes);
                seconds.innerText = pad(numberOfSeconds);
            }

            /*             function pad(number) {
                              if (number < 10) {
                                  return '0' + number; }
                              else {
                                  return number; }
                          } */

// Ternary Operator
            function pad(number) {
                return number < 10 ? '0' + number : number;
            }

            $scope.upload = function () {
                $('#fileUploader').dxFileUploader({
                    uploadMode: "useButtons",
                    onInitialized: function (e) {
                        console.log('init uploader', e);
                        uploader = e.component;
                    },
                    onValueChanged: function (e) {
                        console.log('value changed ', e);
                        if (e.value && e.value.length) {
                            var file = e.value[0];
                            $scope.waiting = true;


                            var deferred = $q.defer();
                            $crud('voice')
                                .url("enroll")
                                .type('form')
                                .post({
                                    username: $scope.data.username,
                                    prompt: $scope.data.prompt ? $scope.data.prompt : '',
                                    audio: file
                                })
                                .then(
                                    function (response) {
                                        uploader.option('value', []);
                                        toastr.success('با موفقیت ثبت گردید.');
                                        $mdDialog.hide();
                                        deferred.resolve();
                                    },
                                    function (response) {
                                        toastr.error('خطایی در ارسال فایل به وجود آمده است.');
                                        console.log('DialogController.upload()');
                                        deferred.reject();
                                    }
                                );
                            return deferred.promise;

                        }
                    },
                    onUploaded: function (e) {

                    }
                });
                $timeout(function () {
                    var fileUploader = $('#fileUploader').dxFileUploader('instance');
                    fileUploader._isCustomEvent = true;
                    fileUploader._isCustomClickEvent = true;
                    fileUploader._$fileInput.click();

                }, 1000);

            }


            $scope.reject = function () {
                $mdDialog.cancel();
            };


            // Events

            // Watchers


            // Variables
            var uploader;
            $scope.state = {
                mode: "",
                status: ""
            };

            // grab what we need
            var minutes = document.querySelector('.minutes');
            var seconds = document.querySelector('.seconds');
            var timerTime = 0;
            var isRunning = false;
            var interval = void 0;

            // Expose globally your audio_context, the recorder instance and audio_stream
            var audio_context;
            var recorder;
            var audio_stream;

            var recordBLOB;
            // Initialize
            Initialize()

        }


    }

)();

