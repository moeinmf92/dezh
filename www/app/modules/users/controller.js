(function () {

    'use strict';

    // Define controller
    angular
        .module('dezhafzar.users')
        .controller('UsersController', UsersController);

    // Controller injections
    UsersController.$inject = [
        '$scope',
        '$rootScope',
        '$mdDialog',
        '$q',
        '$timeout',
        '$crud',
        'toastr'
    ];

    // Controller object
    function UsersController($scope,$rootScope , $mdDialog, $q, $timeout, $crud, toastr) {

        // Functions
        var Initialize = function () {

        };
        $scope.toggle = function (item, list) {
            var idx = list.indexOf(item);
            if (idx > -1) {
                list.splice(idx, 1);
            }
            else {
                list.push(item);
            }
        }
        $scope.exists = function (item, list) {
            return list.indexOf(item) > -1;
        };
        $scope.isChecked = function () {
            return $scope.selected.length === $scope.items.length;
        };
        $scope.toggleAll = function () {
            if ($scope.selected.length === $scope.items.length) {
                $scope.selected = [];
            } else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
                $scope.selected = $scope.items.slice(0);
            }
        };
        $scope.GetUsersList = function (query) {
            var deferred = $q.defer();
            if (!angular.equals($scope.DefaultfilterModel, {})) {
                var params = angular.copy($scope.usersList.params);
                var data = angular.merge(params, $scope.DefaultfilterModel);
                // data = angular.merge(data, {'quick': $scope.quickSearch});
                $crud('users')
                    .url("list")
                    .type('form')
                    .post(data)
                    .then(
                        function (response) {
                            moment.locale('fa');
                            moment.loadPersian({usePersianDigits: true});
                            $scope.items = response.msg.users ? response.msg.users : [];
                            $scope.items.map(function (item) {
                                item.r_created_date_fa = item.created_date ? moment(moment.unix(item.created_date).format("YYYYMMDD HHmmss")).fromNow() : '';
                                item.created_date_fa = moment.unix(item.created_date).format("HH:mm:s jYYYY/jM/jD ");
                            });
                            deferred.resolve({
                                total: response.msg.count,
                                items: $scope.items
                            });
                        },
                        function (response) {
                            console.log('UsersController.GetUsersList()');
                            deferred.reject(response);
                        }
                    );
            }
            else
                deferred.resolve({
                    total: 0,
                    items: []
                });
            return deferred.promise;
        };
        $scope.updateUser = function (user) {
            var data = user ? angular.copy(user) : {};
            console.log("send params in ctrl", data, user);
            $mdDialog
                .show({
                    parent: angular.element(document.body),
                    controller: 'UpdateUserController',
                    templateUrl: 'app/modules/users/dialogs/updateUser.html',
                    clickOutsideToClose: false,
                    locals: {data: data}
                })
                .then(
                    function (response) {
                        console.log('R R S ', response);
                        $scope.usersList.getList();
                    },
                    function (response) {
                        console.log('R R F ', response)
                    }
                )
        };
        $scope.references = function (user) {
            var data = user ? angular.copy(user) : {};
            console.log("send params in ctrl", data);
            $mdDialog
                .show({
                    parent: angular.element(document.body),
                    controller: 'ReferencesController',
                    templateUrl: 'app/modules/users/dialogs/references.html',
                    clickOutsideToClose: false,

                    multiple: true,
                    locals: {data: data}
                })
                .then(
                    function (response) {
                        console.log('R R S ', response);
                        // $scope.usersList.getList();
                    },
                    function (response) {
                        console.log('R R F ', response)
                    }
                )
        };
        $scope.evaluations = function (user) {
            var data = user ? angular.copy(user) : {};
            console.log("send params in ctrl", data);
            $mdDialog
                .show({
                    parent: angular.element(document.body),
                    controller: 'EvaluationsController',
                    templateUrl: 'app/modules/users/dialogs/evaluations.html',
                    clickOutsideToClose: false,

                    multiple: true,
                    locals: {data: data}
                })
                .then(
                    function (response) {
                        console.log('R R S ', response);
                        // $scope.usersList.getList();
                    },
                    function (response) {
                        console.log('R R F ', response)
                    }
                )
        };
        $scope.deleteUser = function (user) {
            console.log("deleteUser", user)
            var deferred = $q.defer();
            $mdDialog
                .show({
                    parent: angular.element(document.body),
                    controller: 'ConfirmController',
                    templateUrl: 'app/common/dialogs/confirm/partial.html',
                    clickOutsideToClose: false,
                    locals: {data: {description: 'آیا از حذف این کاربر اطمینان دارید ؟ ', hint: ''}}
                })
                .then(
                    function (response){
                        $crud('users')
                            .url('remove')
                            .get({username: user.username})
                            .then(
                                function (response) {
                                    toastr.success("کاربر با موفقیت حذف گردید.");
                                    $scope.usersList.getList();
                                    deferred.resolve();
                                },
                                function (response) {
                                    toastr.error('متاسفانه خظایی در حذف کاربر به وجود آمده است.');
                                    console.log('UsersController.deleteUser()');
                                    deferred.reject();
                                });
                    }
                );
            return deferred.promise;
        };

        // Events

        // Watchers
        $scope.$watch('DefaultfilterModel', function (newVal, oldVal) {
            console.log(newVal, oldVal);
            if (!angular.equals(newVal, {})) {
                $scope.usersList.getList();
            }

        }, true);
        $scope.$watch('DefaultfilterModel.created_date_from', function (newVal, oldVal) {
            $scope.fromDatepickerConfig.value = newVal ? Number(newVal + '000') : null;
            $scope.toDatepickerConfig.minDate = newVal ? Number(newVal + '000') : null;
            $scope.toDatepickerConfig.maxDate = $scope.DefaultfilterModel.created_date_to ? $scope.DefaultfilterModel.created_date_to : Date.now();
            $rootScope.$broadcast('datepicker:change', {to: $scope.toDatepickerConfig});
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        });
        $scope.$watch('DefaultfilterModel.created_date_to', function (newVal, oldVal) {
            $scope.toDatepickerConfig.value = newVal ? Number(newVal + '000') : Date.now();
            $scope.fromDatepickerConfig.maxDate = newVal ? Number(newVal + '000') : Date.now();
            $rootScope.$broadcast('datepicker:change', {from: $scope.fromDatepickerConfig});
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        });


        // Variables
        $scope.items = [];
        $scope.selected = [];


        // $scope.quickSearch = "";
        $scope.DefaultfilterModel = {};
        $scope.DefaultfilterConfig = [
            {
                key: 'fullname',
                name: 'نام کامل',
                value: '',
                type: 'string'
            },
            {
                key: 'username',
                name: 'نام کاربری',
                value: '',
                type: 'string'
            },
            {
                key: 'created_date',
                name: 'تاریخ ایجاد',
                value: '',
                type: 'date'
            },

        ];
        $scope.fromDatepickerConfig = {
            "format": "LLLL",
            "viewMode": "day",
            "minDate": null,
            "maxDate": Date.now(),
            "onlyTimePicker": false,
            "initialValue": false,
            "title": 'انتخاب تاریخ شروع'
        };
        $scope.toDatepickerConfig = {
            "format": "LLLL",
            "viewMode": "day",
            "minDate": null,
            "maxDate": Date.now(),
            "onlyTimePicker": false,
            "initialValue": false,
            "title": 'انتخاب تاریخ پایان'
        };
        $scope.filterEnable = true;


        // Initialize
        Initialize()

    }

})();
