(function() {

    'use strict';

    // Define module
    angular
        .module('dezhafzar.users', [])
        .config(function($menuProvider, $stateProvider) {

            // Menu config
            $menuProvider.set('user', {
                id: '2',
                sref: 'panel.users',
                text: 'کاربران',
                icon: {
                    type: 'material',
                    text: 'supervisor_account'
                }
            });

            // State config
            $stateProvider
                .state('panel.users', {
                    url: '^/users',
                    views: {
                        'module': {
                            templateUrl: 'app/modules/users/partial.html',
                            controller: 'UsersController'
                        }
                    },
                    data: {
                        meta: {
                            'titleSuffix': ' | کاربران'
                        }
                    }
                });

        });

})();