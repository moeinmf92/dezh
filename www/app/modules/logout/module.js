(function() {

    'use strict';

    // Define module
    angular
        .module('dezhafzar.logout', [])
        .config(function ($menuProvider, $stateProvider) {

            // Menu config
            $menuProvider.set('user', {
                id: '10',
                sref: 'logout',
                text: 'خروج',
                icon: {
                    type: 'ion',
                    text: 'ion-log-out'
                }
            });

            // State config
            $stateProvider
                .state('logout', {
                    url: '/logout',
                    onEnter: function($authentication, toastr) {
                        $authentication
                            .logout()
                            .then(function (response) {
                                toastr.success('خروج از حساب کاربری با موفقیت انجام شد.');
                            });
                    }
                });
        });

})();