(function () {

    'use strict';

    // Define controller
    angular
        .module('dezhafzar.dashboard')
        .controller('DashboardController', DashboardController);

    // Controller injections
    DashboardController.$inject = [
        '$scope',
        '$q',
        '$url',
        '$timeout',
        '$crud',
        'toastr'
    ];

    // Controller object
    function DashboardController($scope, $q, $url, $timeout, $crud, toastr) {

        // Functions
        var Initialize = function () {

        };

        // Events

        // Watchers

        // Variables

        // Initialize
        Initialize()

    }

})();
