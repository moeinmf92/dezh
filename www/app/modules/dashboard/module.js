(function() {

    'use strict';

    // Define module
    angular
        .module('dezhafzar.dashboard', [])
        .config(function($menuProvider, $stateProvider) {
            var permission = '';

            // Menu config
            $menuProvider.set('user', {
                id: '1',
                parent_id : '',
                have_child : false,
                sref: 'panel.dashboard',
                text: 'داشبورد',
                icon: {
                    type: 'material',
                    text: 'dashboard'
                },
                permission : permission
            });

            // State config
                $stateProvider
                    .state('panel.dashboard', {
                        url: '^/dashboard',
                        views: {
                            'module': {
                                templateUrl: 'app/modules/dashboard/partial.html',
                                controller: 'DashboardController'
                            }
                        },
                        data: {
                            meta: {
                                'titleSuffix': ' | داشبورد'
                            }
                        },
                        // resolve: {
                        //     preview : function ($state, $stateParams, $q,$rootScope) {
                        //         var deferred = $q.defer();
                        //         if(angular.equals($rootScope.loaded,false))
                        //             deferred.reject();
                        //         else
                        //             deferred.resolve();
                        //         deferred
                        //             .promise
                        //             .catch(
                        //                 function(){
                        //                     $rootScope.$on('InitializedDictionary',function (value) {
                        //                         $state.go('panel.dashboard');
                        //                         return deferred.promise;
                        //                     });
                        //                 }
                        //             );
                        //         return deferred.promise;
                        //     }}
                    });

        });

})();