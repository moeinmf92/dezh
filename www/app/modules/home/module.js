(function () {

    'use strict';

    // Define module
    angular
        .module('dezhafzar.default', [])
        .config(function ($menuProvider, $stateProvider) {

            // Menu config

            // State config
            $stateProvider
                .state('home.default', {
                    url: '^/',
                    views: {
                        'nested@home': {
                            templateUrl: 'app/modules/home/partial.html',
                            controller: 'DefaultController'
                        }
                    },
                    data: {
                        meta: {
                            'titleSuffix': ' | صفحه ی اصلی'
                        }
                    }
                });

        });

})();