(function () {

    'use strict';

    // Define controller
    angular
        .module('dezhafzar.default')
        .controller('DefaultController', DefaultController);

    // Controller injections
    DefaultController.$inject = [
        '$scope',
        '$authentication',
        '$url',
        '$auth',
        '$state',
        '$q',
        '$crud',
        '$mdDialog',
        'toastr',
        '$timeout'
    ];

    // Controller object
    function DefaultController($scope, $authentication, $url, $auth, $state, $q, $crud, $mdDialog, toastr, $timeout) {

        // Functions
        var Initialize = function () {
            RecorderInitialize();
        };

        const left = document.querySelector(".left");
        const right = document.querySelector(".right");
        const container = document.querySelector(".container");

        left.addEventListener("mouseenter", () => {
            container.classList.add("hover-left");
        });

        left.addEventListener("mouseleave", () => {
            container.classList.remove("hover-left");
        });

        right.addEventListener("mouseenter", () => {
            container.classList.add("hover-right");
        });

        right.addEventListener("mouseleave", () => {
            container.classList.remove("hover-right");
        });


        $scope.reset = function () {
            $scope.state.mode = '';
            $scope.state.status = '';
            $scope.state.type = '';
            $scope.data = {};
            resetTimer();
        };


        $scope.login = function () {
            if (audio_context && audio_context.state !== 'closed')
                stopRecording();
            if (!$authentication.isLogin())
                $state.go('home.login');
            else
                $state.go('panel.dashboard');
        };
        $scope.start = function () {
            if ($scope.data.user)
                getPrompt().then(function () {
                    $scope.state.type = 'record';
                    $scope.state.status = 'record';
                    $timeout(function () {
                        $scope.record();
                    }, 200);

                },function () {

                });
            else
                toastr.warning('لطفا ابتدا کاربر را انتخاب نمایید.')


        };
        var getPrompt = function () {
            /**
             * GET PROMPT TEXTS
             */
            var deferred = $q.defer();
            $crud('recognition')
                .url('prompt')
                .get({username: $scope.data.user})
                .then(
                    function (response) {
                        $scope.data.prompt = response.msg.prompt;
                        $scope.data.words = response.msg.prompt.split(' ');
                        deferred.resolve();
                    },
                    function (response) {
                        if (response)
                            toastr.error('متاسفانه خظایی در دریافت متن به وجود آمده است.');
                        console.log('HomeController.getPrompt()');
                        deferred.reject();
                    }
                );
            return deferred.promise;
        };
        var authenticate = function (AudioBLOB) {
            var deferred = $q.defer();
            $scope.state.status = 'result';
            $crud('authenticate-voice')
                .type('form')
                .post({
                    username: $scope.data.user,
                    audio: AudioBLOB
                })
                .then(
                    function (response) {
                        response.msg.status = Number(response.msg.status);
                        $scope.data.status_code = response.msg.status;
                        switch (response.msg.status) {
                            case 202:
                                $scope.data.result = 'تایید شد.';
                                break;
                            case 203:
                                $scope.data.result = 'متن نامعتبر';
                                break;
                            case 401:
                                $scope.data.result = 'عدم تایید';
                                break;
                            case 415:
                                $scope.data.result = 'صوت نامعتبر';
                                break;
                            default:
                                $scope.data.result = 'خطا در برقراری ارتباط';
                                break;
                        }
                        deferred.resolve();
                    },
                    function (response) {
                        $scope.state.status = '';
                        console.log('DefaultController.authenticate()');
                        deferred.reject();
                    }
                );
            return deferred.promise;
        };
        $scope.record = function () {

            // startRecording();
            console.log('start');

            progress(8, 8, $('#progressBar'), function (callback) {
                console.log('end');
                // stopRecording(authenticate, null);
            });
            // if (init_audio && audio_context && audio_context.state !== 'closed') {
            //     if (!$scope.power) {
            //         $scope.second = 8;
            //         $scope.power = true;
            //
            //         intervalFn = setInterval(function () {
            //             if ($scope.second !== 0) {
            //                 $scope.second = $scope.second - 1;
            //                 $scope.$apply();
            //             }
            //             else {
            //                 stopRecording(authenticate, null);
            //                 clearInterval(intervalFn);
            //                 // authenticate();
            //                 $scope.power = false;
            //                 $scope.$apply();
            //             }
            //
            //         }, 1000)
            //     }
            // }
            // else if (init_audio && audio_context && audio_context.state === 'closed') {
            //     audio_context = new AudioContext;
            //     $scope.record();
            // }

        };


        $scope.identifyUpload = function () {
            $scope.state.type = 'upload';
            $('#fileUploader').dxFileUploader({
                uploadMode: "useButtons",
                onInitialized: function (e) {
                    console.log('init uploader', e);
                    uploader = e.component;
                },
                onValueChanged: function (e) {
                    console.log('value changed ', e);
                    if (e.value && e.value.length) {
                        var file = e.value[0];
                        $scope.waiting = true;


                        var deferred = $q.defer();
                        $crud('recognition')
                            .url("identify")
                            .type('form')
                            .post({
                                username: "demoUser",
                                audio: file
                            })
                            .then(
                                function (response) {
                                    $scope.state.status = 'result';
                                    $scope.data.users = response.msg.results;
                                    uploader.option('value', []);
                                    toastr.success('با موفقیت ثبت گردید.');
                                    $scope.waiting = false;
                                    deferred.resolve();
                                },
                                function (response) {
                                    $scope.state.type = '';
                                    toastr.error('خطایی در ارسال فایل به وجود آمده است.');
                                    console.log('DefaultController.identifyUpload()');
                                    deferred.reject();
                                }
                            );
                        return deferred.promise;


                        //
                        // var payload = new FormData();
                        //
                        //
                        // payload.append('file', file);
                        // payload.append('request_id', $scope.active.req_id);
                        // payload.append('scale', scale);
                        // payload.append('format', 'dwg');
                        // payload.append('type', type);
                        //
                        // return $http({
                        //     url: $url.endpoint('autocad').url + "/upload",
                        //     method: 'POST',
                        //     data: payload,
                        //     //assign content-type as undefined, the browser
                        //     //will assign the correct boundary for us
                        //     headers: {
                        //         'Content-Type': undefined,
                        //         "Authorization": "JWT " + $auth.getToken()
                        //     },
                        //     //prevents serializing payload.  don't do it.
                        //     transformRequest: angular.identity
                        // }).then(function (response) {
                        //     uploader.option('value', []);
                        //     toastr.success('با موفقیت ثبت گردید.');
                        //     $scope.waiting = false;
                        //     $scope.data[$scope.data.length - 1].files[type].dwg['s' + scale] = true;
                        //
                        // }, function (response) {
                        //     uploader.option('value', []);
                        //     toastr.error('خطایی در ثبت اطلاعات به وجود آمده است.');
                        //     console.log('AcqLandsDialogController.uploadCAD()');
                        //     $scope.waiting = false;
                        //
                        // });

                    }
                },
                onUploaded: function (e) {

                }
            });
            $timeout(function () {
                var fileUploader = $('#fileUploader').dxFileUploader('instance');
                fileUploader._isCustomEvent = true;
                fileUploader._isCustomClickEvent = true;
                fileUploader._$fileInput.click();

            }, 500);

        };


        $scope.stopRecord = function () {
            $scope.state.status = 'stop';
            resetTimer();
            stopTimer();
            if (angular.equals(recordBLOB, {}))
                stopRecording(function (AudioBLOB) {
                    recordBLOB = AudioBLOB;
                    console.log("stopRecord");
                    var deferred = $q.defer();
                    $mdDialog
                        .show({
                            parent: angular.element(document.body),
                            controller: 'ConfirmController',
                            templateUrl: 'app/common/dialogs/confirm/partial.html',
                            clickOutsideToClose: false,
                            multiple: true,
                            locals: {data: {description: ' تمایل به ثبت این صوت دارید ؟ ', hint: ''}}
                        })
                        .then(
                            function (response) {
                                var deferred = $q.defer();
                                $crud('recognition')
                                    .url("identify")
                                    .type('form')
                                    .post({
                                        username: "demoUser",
                                        audio: recordBLOB
                                    })
                                    .then(
                                        function (response) {
                                            $scope.state.status = 'result';
                                            debugger
                                            $scope.data.users = response.msg.results;
                                            // toastr.success('با موفقیت ثبت گردید.');
                                            // $mdDialog.hide();
                                            deferred.resolve();
                                        },
                                        function (response) {
                                            toastr.error('خطایی در ارسال صوت به وجود آمده است.');
                                            console.log('DialogController.stopRecord()');
                                            deferred.reject();
                                        }
                                    );
                                return deferred.promise;

                            }
                        );
                    return deferred.promise;


                }, null);

            else {


                console.log("stopRecord");
                var deferred = $q.defer();
                $mdDialog
                    .show({
                        parent: angular.element(document.body),
                        controller: 'ConfirmController',
                        templateUrl: 'app/common/dialogs/confirm/partial.html',
                        clickOutsideToClose: false,
                        multiple: true,
                        locals: {data: {description: ' تمایل به ثبت این صوت دارید ؟ ', hint: ''}}
                    })
                    .then(
                        function (response) {
                            var deferred = $q.defer();
                            $crud('recognition')
                                .url("identify")
                                .type('form')
                                .post({
                                    username: "demoUser",
                                    audio: recordBLOB
                                })
                                .then(
                                    function (response) {
                                        $scope.state.status = 'result';
                                        $scope.data.users = response.msg.results;
                                        // toastr.success('با موفقیت ثبت گردید.');
                                        // $mdDialog.hide();
                                        deferred.resolve();
                                    },
                                    function (response) {
                                        toastr.error('خطایی در ارسال صوت به وجود آمده است.');
                                        console.log('DialogController.stopRecord()');
                                        deferred.reject();
                                    }
                                );
                            return deferred.promise;

                        }
                    );
                return deferred.promise;


            }


        };

        $scope.resetRecord = function () {
            $scope.state.type = "record";
            $scope.state.status = "record";
            recordBLOB = {};
            resetTimer();
            startTimer();
            startRecording();

        };

        $scope.identifyRecord = function () {

            $scope.state.type = "record";
            $scope.state.status = "record";


            minutes = document.querySelector('.minutes');
            seconds = document.querySelector('.seconds');

            recordBLOB = {};
            startTimer();
            startRecording();


        };

        function startTimer() {
            if (isRunning) return;

            isRunning = true;
            interval = setInterval(incrementTimer, 1000);
        }

        function stopTimer() {
            if (!isRunning) return;

            isRunning = false;
            clearInterval(interval);
        }

        function resetTimer() {
            timerTime = 0;
            minutes.innerText = '00';
            seconds.innerText = '00';
        }

        function incrementTimer() {
            timerTime++;

            var numberOfMinutes = Math.floor(timerTime / 60);
            var numberOfSeconds = timerTime % 60;

            minutes.innerText = pad(numberOfMinutes);
            seconds.innerText = pad(numberOfSeconds);
        }


        // Ternary Operator
        function pad(number) {
            return number < 10 ? '0' + number : number;
        }


        var exportAudio = function (AudioBLOB) {
            // Note:
            // Use the AudioBLOB for whatever you need, to download
            // directly in the browser, to upload to the server, you name it !

            // In this case we are going to add an Audio item to the list so you
            // can play every stored Audio

            var filename = new Date().toISOString() + '.wav';
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(AudioBLOB);
            link.download = filename;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        };


        /**
         * Patch the APIs for every browser that supports them and check
         * if getUserMedia is supported on the browser.
         *
         */
        function RecorderInitialize() {
            try {


                navigator.getUserMedia = (
                    navigator.getUserMedia ||
                    navigator.webkitGetUserMedia ||
                    navigator.mozGetUserMedia ||
                    navigator.msGetUserMedia
                );

                if (typeof navigator.mediaDevices.getUserMedia === 'undefined') {
                    navigator.getUserMedia({
                        audio: true
                    }, function () {


                        // Monkeypatch for AudioContext, getUserMedia and URL
                        window.AudioContext = window.AudioContext || window.webkitAudioContext;
                        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia;
                        window.URL = window.URL || window.webkitURL;

                        // Store the instance of AudioContext globally
                        init_audio = true;
                        audio_context = new AudioContext;
                        console.log('Audio context is ready !');
                        console.log('navigator.getUserMedia ' + (navigator.getUserMedia ? 'available.' : 'not present!'));


                    }, function () {
                        alert('No web audio support in this browser!');
                    });
                } else {
                    navigator.mediaDevices.getUserMedia({
                        audio: true
                    }).then(function () {

                        // Monkeypatch for AudioContext, getUserMedia and URL
                        window.AudioContext = window.AudioContext || window.webkitAudioContext;
                        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia;
                        window.URL = window.URL || window.webkitURL;

                        // Store the instance of AudioContext globally
                        init_audio = true;
                        audio_context = new AudioContext;
                        console.log('Audio context is ready !');
                        console.log('navigator.getUserMedia ' + (navigator.getUserMedia ? 'available.' : 'not present!'));


                    }).catch(function () {
                        alert('No web audio support in this browser!');
                    });
                }


            } catch (e) {
                alert('No web audio support in this browser!');
            }
        }

        /**
         * Starts the recording process by requesting the access to the microphone.
         * Then, if granted proceed to initialize the library and store the stream.
         *
         * It only stops when the method stopRecording is triggered.
         */
        function startRecording() {
            // Access the Microphone using the navigator.getUserMedia method to obtain a stream

            navigator.getUserMedia({audio: true}, function (stream) {

                var paths = document.getElementsByTagName('path');
                var visualizer = document.getElementById('visualizer');
                var mask = visualizer.getElementById('mask');
                var path;

                // Expose the stream to be accessible globally
                audio_stream = stream;
                // Create the MediaStreamSource for the Recorder library
                var input = audio_context.createMediaStreamSource(stream);
                console.log('Media stream succesfully created');


                // window.persistAudioStream = stream;
                // var audioStream = audio_context.createMediaStreamSource(stream);
                var analyser = audio_context.createAnalyser();
                input.connect(analyser);
                analyser.fftSize = 1024;

                var frequencyArray = new Uint8Array(analyser.frequencyBinCount);
                visualizer.setAttribute('viewBox', '0 0 255 255');

                //Through the frequencyArray has a length longer than 255, there seems to be no
                //significant data after this point. Not worth visualizing.
                for (var i = 0; i < 255; i++) {
                    path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
                    path.setAttribute('stroke-dasharray', '4,1');
                    mask.appendChild(path);
                }
                var doDraw = function () {
                    requestAnimationFrame(doDraw);
                    analyser.getByteFrequencyData(frequencyArray);
                    var adjustedLength;
                    for (var i = 0; i < 255; i++) {
                        if (paths[i]) {
                            adjustedLength = Math.floor(frequencyArray[i]) - (Math.floor(frequencyArray[i]) % 5);
                            paths[i].setAttribute('d', 'M ' + (i) + ',255 l 0,-' + adjustedLength);
                        }
                        else break

                    }

                };
                doDraw();


                // Initialize the Recorder Library
                recorder = new Recorder(input, {numChannels: 1});
                console.log('Recorder initialised');

                // Start recording !
                recorder && recorder.record();
                console.log('Recording...');

                // Disable Record button and enable stop button !
                // document.getElementById("start-btn").disabled = true;
                // document.getElementById("stop-btn").disabled = false;
            }, function (e) {
                console.error('No live audio input: ' + e);
            });
        }

        /**
         * Stops the recording process. The method expects a callback as first
         * argument (function) executed once the AudioBlob is generated and it
         * receives the same Blob as first argument. The second argument is
         * optional and specifies the format to export the blob either wav or mp3
         */
        function stopRecording(callback, AudioFormat) {
            // Stop the recorder instance
            // audio_context.close();
            recorder && recorder.stop();

            console.log('Stopped recording.');

            if (audio_stream)
            // Stop the getUserMedia Audio Stream !
                audio_stream.getAudioTracks()[0].stop();

            // Disable Stop button and enable Record button !
            // document.getElementById("start-btn").disabled = false;
            // document.getElementById("stop-btn").disabled = true;

            // Use the Recorder Library to export the recorder Audio as a .wav file
            // The callback providen in the stop recording method receives the blob
            if (typeof(callback) === "function") {
                /**
                 * Export the AudioBLOB using the exportWAV method.
                 * Note that this method exports too with mp3 if
                 * you provide the second argument of the function
                 */
                recorder && recorder.exportWAV(function (blob) {
                    callback(blob);
                    // create WAV download link using audio data blob
                    // createDownloadLink();
                    // Clear the Recorder to start again !
                    recorder.clear();
                }, 16000, (AudioFormat || "audio/wav"));
            }
        }


        function progress(timeleft, timetotal, $element, callback) {
            var progressBarWidth = timeleft * $element.width() / timetotal;
            // $element.find('div').animate({width: progressBarWidth}, 500).html(Math.floor(timeleft / 60) + ":" + timeleft % 60);
            $element.find('div').animate({width: progressBarWidth}, 1000);
            if (timeleft > 0) {
                setTimeout(function () {
                    progress(timeleft - 1, timetotal, $element, callback);
                }, 1000);
            }
            else {
                if (typeof callback === 'function')
                    callback();
            }
        }


        // Watchers

        // Variables
        $scope.state = {
            mode: '',
            type: '',
            status: ''
        };

        var minutes = document.querySelector('.minutes');
        var seconds = document.querySelector('.seconds');
        var timerTime = 0;
        var isRunning = false;
        var interval = void 0;
        // Expose globally your audio_context, the recorder instance and audio_stream
        var audio_context;
        var recorder, uploader;
        var audio_stream;
        var init_audio = false;
        var intervalFn = null;
        $scope.power = false;
        $scope.pending = true;
        $scope.data = {
            user: ""
        };
        $scope.avatarDir = $url.endpoint('get_avatar').url;
        $scope.today = new Date();
        $scope.users = [];
        var recordBLOB = {};

        // Initialize
        Initialize()

    }

})();
