(function (server) {

    'use strict';

    // Define application environment
    angular
        .module('dezhafzar.config', [])
        .constant('_client', server._client)
        .constant('_api', server._api)
        .constant('_endpoint', server._endpoint)
        .constant('_regex', server._regex)
        .constant('_point_types', server._point_types)
        .constant('_cities', server._cities);


})(_server);