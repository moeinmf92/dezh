var _server = {

    _client: {
        id: '1',
        secret: 'Xhvggig83eLpOpQUVrLky3p4jTsO8EDGv2wTvQHD'
    },
    _api: {
        // local: 'https://dezhafzar.com',

        dezhafzar: {
            host: 'http://95.38.21.180',
            port: 5002,
            path: ''
        }
    },
    _endpoint: {
        'token': {
            api: 'oauth',
            path: 'auth/token',
            token: false
        },
        'admin-login': {
            api: 'dezhafzar',
            path: 'generateToken ',
            token: false
        },
        'admin-logout': {
            api: 'digipeyk',
            path: 'admin/logout',
            token: false
        },
        'recognition': {
            api: 'dezhafzar',
            path: 'recognition',
            token: true
        },
        'authenticate-voice':{
            api: 'dezhafzar',
            path: 'recognition/authenticate',
            token: false
        },
        'users': {
            api: 'dezhafzar',
            path: 'user',
            token: true
        },
        'logs': {
            api: 'dezhafzar',
            path: 'log',
            token: true
        },
        'prompt': {
            api: 'dezhafzar',
            path: 'prompt',
            token: true
        },
        'voice': {
            api: 'dezhafzar',
            path: 'voice',
            token: true
        },
        'ref-download': {
            api: 'dezhafzar',
            path: 'voice/ref/download',
            type : 'blob',
            token: true
        },
        'eval-download': {
            api: 'dezhafzar',
            path: 'voice/eval/download',
            type : 'blob',
            token: true
        },
        'refs': {
            api: 'dezhafzar',
            path: 'voice/ref',
            token: true
        },
        'evals': {
            api: 'dezhafzar',
            path: 'voice/eval',
            token: true
        },
        'get_avatar': {
            api: 'dezhafzar',
            path: 'user/avatar',
            token: true
        },
    }
};

