(function () {

    'use strict';

    // Define service
    angular
        .module('ng')
        .service('$authentication', $authentication);

    // Service injections
    $authentication.$inject = [
        '$rootScope',
        '$state',
        '$localStorage',
        '$q',
        '$url',
        '$mdDialog',
        '$auth',
        '$crud',
        'toastr',
        '$location',
        '$timeout'
    ];

    // Service objects
    function $authentication($rootScope, $state, $localStorage, $q, $url, $mdDialog, $auth, $crud, toastr, $location, $timeout) {

        // Functions
        var initialize = function () {
            user = object();
            if (getToken()) {
                getLocalStorage();
            }
        };

        var object = function () {
            return {
                info: {},
                roles: [],
                permissions: [],
                msgs: [],
                reminders : []
            }
        };
        var dynamicSort = function(property) {
            var sortOrder = -1;
            return function (a, b) {
                var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
                return result * sortOrder;
            }
        };
        var fillProfile = function (arg) {
            user = angular.copy(arg);
            // debugger
            if (arg.roles.length && !user['activeRole'])
                user['activeRole'] = arg.roles[0];
// debugger

            fillLocalStorage();
        };
        var fillLocalStorage = function () {
            $localStorage.dezhafzarUser = user;


            $rootScope.$broadcast('$authentication:user');
        };
        var getLocalStorage = function () {

            user = angular.copy($localStorage.dezhafzarUser) || {};
            // sync();

        };
        var get = function (arg) {
            if (user) {
                if (arg === undefined)
                    return angular.copy(user);
                return angular.copy(user[arg]) || '';
            }
            return undefined;
        };
        var sync = function () {
            var deferred = $q.defer();
            if (!syncing) {
                syncing = true;
                $crud('profile')
                    .get()
                    .then(
                        function (response) {
                            syncing = false;
                            fillProfile(response.msg);
                            deferred.resolve();
                        },
                        function (response) {
                            syncing = false;
                            // fillProfile(response.msg);
                            console.log('$authentication.sync()');
                            deferred.reject();
                        }
                    );
            }
            return deferred.promise;
        };

        var logout = function () {
            var deferred = $q.defer();
            $auth
                .logout()
                .then(
                    function (response) {
                        $localStorage.$reset();
                        $mdDialog.cancel();
                        $state.go('home.login');
                        deferred.resolve(response);
                    },
                    function (response) {
                        console.log('$authentication.logout()');
                        deferred.reject(response);
                    }
                );
            return deferred.promise;
        };
        var login = function (arg) {
            var deferred = $q.defer();
            console.log(JSON.stringify(arg), arg);
            $auth
                .login(
                    JSON.stringify(arg),
                    {
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    }
                )
                .then(
                    function (response) {
                        // sync().then(function () {
                        //     toastr.success('ورود شما با موفقیت انجام شد.');
                            deferred.resolve(response);
                        // })
                    },
                    function (response) {
                        console.log('$authentication.login()');
                        deferred.reject(response);
                    }
                );
            return deferred.promise;
        };
        var isLogin = function () {
            return $auth.isAuthenticated();
        };
        var refreshToken = function () {
            if (!refreshing) {
                refreshing = true;
                $auth
                    .login(
                        {
                            grant_type: 'refresh_token',
                            refresh_token: getRefreshToken()
                        },
                        {
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                        }
                    )
                    .then(
                        function (response) {
                            $rootScope.$broadcast('$crud:retry');
                            refreshing = false;
                        },
                        function (response) {
                            logout();
                            refreshing = false;
                        }
                    );
            }
        };
        var getToken = function () {
            return $auth.getFullToken();
        };
        var getRefreshToken = function () {
            return $auth.getRefreshToken();
        };
        var setToken = function (token, state) {
            $auth.setToken(token);
            $state.go(state);
        };



        // Events
        $rootScope.$on('$authentication:refresh_token', function (event, data) {
            refreshToken();
        });
        $rootScope.$on('$authentication:logout', function (event, data) {
            logout();
        });

        // Variables
        var socket;
        var user = {},
            syncing = false;
        var refreshing = false,
            service = {
                get: get,
                sync: sync,
                logout: logout,
                login: login,
                isLogin: isLogin,
                getToken: getToken,
                getRefreshToken: getRefreshToken,
                setToken: setToken
            };

        // Initialize
        initialize();

        // Output
        return service;

    }

})();