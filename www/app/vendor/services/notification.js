(function () {

    'use strict';

    // Define service
    angular
        .module('ng')
        .service('$notification', $notification);

    // Service injections
    $notification.$inject = [
        'toastr'
    ];

    // Service objects
    function $notification(toastr) {

        // Functions
        var run = function () {
            if ('Notification' in window) {
                if (!(permission = (Notification.permission === 'granted')))
                    Notification
                        .requestPermission(
                            function (response) {
                                if (response === 'granted')
                                    permission = true;
                            }
                        );
            }
        };
        var notify = function (title, message) {
            sound();
            if (permission)
                notification(title, message);
            else
                toast(title, message);
        };
        var sound = function () {
            (new Audio(path.sound)).play();
        };
        var notification = function (title, message) {
            new Notification(title, {
                body: message,
                dir: dir,
                icon: path.icon
            });
        };
        var toast = function (title, message) {
            toastr.success(title, message);
        };

        // Variables
        var permission = false,
            dir = 'rtl',
            path = {
                icon: 'assets/img/favicons/android-icon-72x72.png',
                sound: 'assets/sound/notification-dezhafzar.ogg'
            };

        // Output
        return {
            run: run,
            notify: notify
        };
    }

})();