(function () {

    'use strict';

    // Define service
    angular
        .module('ng')
        .service('$crud', $crud)
        .service('$url', $url)
        .service('$push', $push);

    // Service injections
    $crud.$inject = [
        '$rootScope',
        '$q',
        '$http',
        '$auth',
        '$url',
        'toastr',
        '$mdDialog',
        '$localStorage'
    ];
    $url.$inject = [
        '$state',
        '_api',
        '_endpoint'
    ];
    $push.$inject = [
        '$rootScope',
        '_api'
    ];

    // Service objects
    function $crud($rootScope, $q, $http, $auth, $url, toastr, $mdDialog, $localStorage) {

        // Functions
        var header = function (arg) {
            http = angular.merge(http, arg);
            return service;
        };
        var getHeaders = function () {
            var header = {};
            if (endpoint.token && $auth.getToken())
                header['Authorization'] = 'JWT ' + $auth.getToken();
            return angular.merge(header, http);
        };

        $rootScope.safeApply = function (fn) {
            var phase = this.$root.$$phase;
            if (phase == '$apply' || phase == '$digest') {
                if (fn && (typeof(fn) === 'function')) {
                    fn();
                }
            } else {
                this.$apply(fn);
            }
        };


        var query = function (method, item, data) {

            var deferred = $q.defer();
            $rootScope.waitingFlag = true;
            var activeRole = $localStorage.dezhafzarUser ? $localStorage.dezhafzarUser.activeRole : null;

            if (method === 'POST')
                if (activeRole)
                    data = (data !== undefined) ? angular.merge(data, {active_role_id: activeRole.id}) : {active_role_id: activeRole.id};
                else
                    data = (data !== undefined) ? data : null;
            else if (method === 'GET')
                if (activeRole)
                    item = (item !== undefined) ? angular.merge(item, {active_role_id: activeRole.id}) : {active_role_id: activeRole.id};
                else
                    item = (item !== undefined) ? item : null;


            switch (strategy) {
                case 'form':
                    var request = new XMLHttpRequest();
                    var payload = new FormData();
                    var url = endpoint.url ? endpoint.url : endpoint;
                    if (method !== 'GET') {
                        angular.forEach(data, function (value, key) {
                            payload.append(key, value);
                        });
                    }
                    else {
                        var params = Object.keys(item).map(function (k) {
                            return encodeURIComponent(k) + '=' + encodeURIComponent(item[k])
                        }).join('&')
                        url = params ? url + '?' + params : url;
                    }
                    request.open(method, url, true);

                    if (endpoint.type === 'blob')
                        request.responseType = 'blob';
                    angular.forEach(getHeaders(), function (value, key) {
                        request.setRequestHeader(key, value);
                    });

                    // request.setRequestHeader("Content-Type", undefined);
                    // request.setRequestHeader("cache-control", "no-cache");

                    request.onload = function () {
                        // Only handle status code 200
                        $rootScope.waitingFlag = false;
                        if (request.status < 400) {
                            http = {};
                            strategy = "";
                            if (endpoint.type !== 'blob')
                                try {
                                    deferred.resolve(JSON.parse(request.responseText));
                                }
                                catch (e) {
                                    deferred.resolve(request.responseText);
                                }
                            else {
                                deferred.resolve(request.response)
                            }


                        }
                        else {
                            http = {};
                            strategy = "";
                            deferred.reject(JSON.parse(request.responseText));
                            if (!request.status)
                                request.status = -1;
                            switch (request.status) {
                                case (401):
                                    if (endpoint.token) {
                                        if ($mdDialog)
                                            $mdDialog.cancel();
                                        logout('زمان اتصال شما به پایان رسیده است ؛ لطفا مجددا وارد سامانه شوید.');
                                    }
                                    break;
                                case (403):
                                    if ($mdDialog)
                                        $mdDialog.cancel();
                                    logout('زمان اتصال شما به پایان رسیده است ؛ لطفا مجددا وارد سامانه شوید.');
                                    break;
                                case (-1):
                                case (500):
                                    disconnect();
                                    break;
                                default:
                            }
                        }
                        // some error handling should be done here...
                    };
                    request.send(payload);
                    break;
                default:
                    $http({
                        url: endpoint.url ? endpoint.url : endpoint,
                        method: method,
                        headers: getHeaders(),
                        params: item,
                        data: data
                    })
                        .then(
                            function (response) {
                                $rootScope.waitingFlag = false;
                                http = {};
                                deferred.resolve(response.data);
                            },
                            function (response) {
                                $rootScope.waitingFlag = false;
                                http = {};
                                if (!item || item.outputFormat !== 'DXF-ZIP') {
                                    if (!response.status)
                                        response.status = -1;
                                    switch (response.status) {
                                        case (401):
                                            if ($mdDialog)
                                                $mdDialog.cancel();
                                            logout('زمان اتصال شما به پایان رسیده است ؛ لطفا مجددا وارد سامانه شوید.');
                                            break;
                                        case (403):
                                            logout('زمان اتصال شما به پایان رسیده است ؛ لطفا مجددا وارد سامانه شوید.');
                                            break;
                                        case (-1):
                                        case (500):
                                            disconnect();
                                            break;
                                        default:
                                    }
                                    console.log('$crud.query()');
                                }
                                deferred.reject(response);
                            }
                        );
            }
            $rootScope.safeApply();
            return deferred.promise;
        };
        var disconnect = function () {
            toastr.error('در حال حاضر ارتباط با سرور مقدور نمی باشد.');
        };
        var refresh = function (method, item, data) {
            storage = {
                method: method,
                item: item,
                data: data
            };
            $rootScope.$broadcast('$authentication:refresh_token');
        };
        var logout = function (arg) {
            toastr.error(arg);
            $rootScope.$broadcast('$authentication:logout');
        };
        var type = function (arg) {
            strategy = arg;
            return service;
        };

        var url = function (arg) {
            endpoint.url += '/' + arg;
            return service;
        };
        var get = function (item) {
            return query('GET', item, null);
        };
        var remove = function (item) {
            return query('DELETE', item, null);
        };
        var patch = function (item) {
            return query('PATCH', null, item);
        };
        var put = function (item) {
            return query('PUT', null, item);
        };
        var post = function (item, params) {
            return query('POST', params ? params : null, item);
        };

        // Events
        $rootScope.$on('$crud:retry', function (event, data) {
            query(storage.method, storage.item, storage.data);
        });

        // Variables
        var http = {};
        var strategy = "";
        var storage = {};
        var endpoint = '';
        var service = {
            type: type,
            url: url,
            get: get,
            remove: remove,
            patch: patch,
            put: put,
            post: post,
            header: header
        };

        // Output
        return function (arg) {
            endpoint = $url.endpoint(arg) ? $url.endpoint(arg) : arg;
            return service;
        };

    }

    function $url($state, _api, _endpoint) {

        // Functions
        var api = function (arg) {
            if (_api[arg]) {
                return (_api.local || domain()) + (_api[arg].port ? ":" + _api[arg].port : "") + '/' + _api[arg].path;
            }

            return undefined
        };
        var endpoint = function (arg) {
            if (_endpoint[arg]) {
                // debugger
                if (_api[_endpoint[arg].api].host) {
                    return {
                        url: _api[_endpoint[arg].api].host + (_api[_endpoint[arg].api].port ? ':' + _api[_endpoint[arg].api].port : '') + '/' + (_api[_endpoint[arg].api].path ? _api[_endpoint[arg].api].path : '') + _endpoint[arg].path,
                        type: _endpoint[arg].type ? _endpoint[arg].type : '',
                        token: _endpoint[arg].token
                    };
                }
                else {
                    return {
                        url: (_api.local || '') + '/' + _api[_endpoint[arg].api].path + '/' + _endpoint[arg].path,
                        type: _endpoint[arg].type ? _endpoint[arg].type : '',
                        token: _endpoint[arg].token
                    };
                }
            }

            return undefined;
        };
        var domain = function () {
            return window.location.protocol + '//' + window.location.hostname;
        };
        var generate = function (state, params) {
            return domain() + $state.href(state, params);
        };

        // Output
        return {
            api: api,
            domain: domain,
            generate: generate,
            endpoint: endpoint
        };
    }

    function $push($rootScope, _api) {

        // Functions
        var pushstream = function () {
            var config = {
                host: _api.push.host,
                port: _api.push.port,
                useSSL: false,
                urlPrefixWebsocket: "/websocket",
                modes: 'websocket|eventsource|stream'
            };
            // if(_api.local)
            //     angular.merge(config, {host: _api.local.split('://')[1]});
            return new PushStream(config);
        };
        var disconnect = function (channel) {
            if (channels[channel] !== undefined) {
                console.log('$push.disconnect()', channel);
                channels[channel].disconnect();
                delete channels[channel];
            }
        };
        var connect = function (channel) {
            if (channels[channel] === undefined) {
                console.log('$push.connect()', channel);
                channels[channel] = pushstream();
                channels[channel].addChannel(channel);
                channels[channel].connect();
                return {
                    receive: function (callback) {
                        if (typeof callback == 'function')
                            channels[channel].onmessage = function (data) {
                                console.log('$push.receive()', channel, JSON.parse(data));
                                callback(JSON.parse(data), channel);
                            };
                    },
                    close: function (callback) {
                        if (typeof callback == 'function')
                            channels[channel].close = function (data) {
                                console.log('$push.close()', channel, JSON.parse(data));
                                callback(JSON.parse(data), channel);
                            };
                    }
                }
            }
            return;
        };

        // Events
        $rootScope.$on('$stateChangeStart', function (data, event) {
            for (var index in channels)
                channels[index] && disconnect(index);
        });

        // Variables
        var channels = {};

        // Output
        return {
            connect: connect,
            disconnect: disconnect
        };
    }
})();
