(function () {

    'use strict';

    // Define service
    angular
        .module('ng')
        .provider('$menu', $menu);

    // Service objects
    function $menu() {

        var menu = {
            guest: {},
            user: {}
        };

        // Output
        return {
            set: function(type, arg){
                if(menu[type] === undefined)
                    return;
                menu[type][arg.id] = arg
            },
            $get: function(){
                return menu;
            }
        }
    }

})();