(function () {

    'use strict';

    // Define directive
    angular
        .module('formValidation', [])
        .directive('formValidation', formValidation);

    // Directive injections
    formValidation.$inject = [
        '$parse'
    ];

    // Directive object
    function formValidation($parse) {
        return {
            restrict: "A",
            scope: false,
            link: function (scope, element, attributes) {

                // Functions
                var Initialize = function () {
                    handlers = $parse(attributes.formValidation)(scope);
                    name = attributes.name;
                    scope[name].lock = false;
                    scope[name].reset = Reset;
                    scope[name].untouched = Untouched;
                    element.attr('novalidate', 'novalidate');
                };
                var Check = function () {
                    if(scope[name].lock)
                        return false;
                    if (scope[name].$pristine) {
                        typeof handlers.pristine == 'function' && handlers.pristine();
                        return false;
                    }
                    if (!scope[name].$valid) {
                        typeof handlers.invalid == 'function' && handlers.invalid();
                        return false;
                    }
                    return true;
                };
                var Submit = function () {
                    scope[name].lock = true;
                    if (typeof handlers.submit == 'function')
                        handlers
                            .submit()
                            .then(
                                function (response) {
                                    scope[name].lock = false;
                                    Clear();
                                }, function (response) {
                                    scope[name].lock = false;
                                }
                            );
                    scope.$apply();
                };
                var Clear = function () {
                    typeof handlers.reset == 'function' && handlers.reset();
                    Untouched();
                };
                var Untouched = function(){
                    scope[name].$setPristine();
                    scope[name].$setUntouched();
                };
                var Reset = function(){
                    !scope[name].lock && Clear();
                };

                // Events
                element.on('submit', function (event) {
                    Check() && Submit();
                });

                // Variables
                var name, handlers;

                // Initialize
                Initialize();

            }
        }
    }

})();