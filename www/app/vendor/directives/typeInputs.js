(function () {
    'use strict';

    // Define directive
    angular
        .module('typeInputs', [])
        .directive('currencyInput', currencyInput)
        .directive('persianInput', persianInput)
        .directive('englishInput', englishInput)
        .directive('fixNumbers', fixNumbers);

    // Directive injections
    currencyInput.$inject = [
        '$filter',
        '$locale'
    ];
    persianInput.$inject = [
        '$parse',
        '$timeout'
    ];
    englishInput.$inject = [
        '$parse',
        '$timeout'
    ];
    fixNumbers.$nject = [
        '$parse',
        '$timeout'
    ];

    // Directive object
    function currencyInput($filter, $locale) {
        var decimalSep = $locale.NUMBER_FORMATS.DECIMAL_SEP;
        var toNumberRegex = new RegExp('[^0-9۰-۹\\' + decimalSep + ']', 'g');
        var filterFunc = function (value) {
            if(!value)
                return '';
            return $filter('currency')(value, '', 0);
        };
        var toNumber = function (currencyStr) {
            var string = switchToEnglish(currencyStr) || '';
            return parseFloat(string.replace(toNumberRegex, ''), 10);
        };
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function postLink(scope, elem, attrs, modelCtrl) {
                modelCtrl.$formatters.push(filterFunc);
                modelCtrl.$parsers.push(function (newViewValue) {
                    var newModelValue = toNumber(newViewValue);
                    modelCtrl.$viewValue = filterFunc(newModelValue);
                    elem.val(modelCtrl.$viewValue);
                    return newModelValue;
                });
            }
        };
    }
    function persianInput($parse, $timeout) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, modelCtrl) {
                var capitalize = function (inputValue) {
                    if (inputValue) {
                        inputValue = switchToPersian(''.concat(inputValue));
                    }
                    modelCtrl.$setViewValue(inputValue);
                    modelCtrl.$render();
                    return inputValue;
                };
                var model = $parse(attrs.ngModel);
                $timeout(
                    function () {
                        if (model(scope))
                            element[0].dispatchEvent(new Event('blur'));
                        modelCtrl.$parsers.push(capitalize);
                        capitalize(model(scope));
                    }, 99
                );
            }
        };
    }
    function englishInput($parse, $timeout) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, modelCtrl) {
                element.on("focusout", function () {
                    modelCtrl.$setViewValue(element.val().replace(/\s+/g, ''));
                    modelCtrl.$render();
                });
                var capitalize = function (inputValue) {
                    if (inputValue)
                        inputValue = switchToEnglish(''.concat(inputValue)).replace(/\s+/g, '');
                    modelCtrl.$setViewValue(inputValue);
                    modelCtrl.$render();
                    return inputValue;
                };
                var model = $parse(attrs.ngModel);
                $timeout(
                    function () {
                        if (model(scope))
                            element[0].dispatchEvent(new Event('blur'));
                        modelCtrl.$parsers.push(capitalize);
                        capitalize(model(scope));
                    }, 99
                );
            }
        };
    }
    function fixNumbers($parse, $timeout) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, modelCtrl) {
                var fix = function (inputValue) {
                    if (inputValue)
                        inputValue = fixPersianNumbers(inputValue);
                    modelCtrl.$setViewValue(inputValue);
                    modelCtrl.$render();
                    return inputValue;
                };
                var model = $parse(attrs.ngModel);
                $timeout(
                    function () {
                        if (model(scope))
                            element[0].dispatchEvent(new Event('blur'));
                        modelCtrl.$parsers.push(fix);
                        fix(model(scope));
                    }, 99
                );
            }
        };
    }

    // Directive requirements
    var persianNumbers = [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g],
        arabicNumbers  = [/٠/g, /١/g, /٢/g, /٣/g, /٤/g, /٥/g, /٦/g, /٧/g, /٨/g, /٩/g],
        persianChar = ["ض", "ص", "ث", "ق", "ف", "غ", "ع", "ه", "خ", "ح", "ج", "چ", "ش", "س", "ی", "ب", "ل", "ا", "ت", "ن", "م", "ک", "گ", "ظ", "ط", "ز", "ر", "ذ", "د", "پ", "و", "؟", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹", "۰"],
        englishChar = ["q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "[", "]", "a", "s", "d", "f", "g", "h", "j", "k", "l", ";", "'", "z", "x", "c", "v", "b", "n", "m", ",", "?", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];
    String.prototype.replaceAll = function (str1, str2, ignore) {
        return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, "\\$&"), (ignore ? "gi" : "g")), (typeof (str2) == "string") ? str2.replace(/\$/g, "$$$$") : str2);
    };
    function fixPersianNumbers(value) {
        if(typeof value === 'string')
            for(var i=0; i<10; i++)
                value = value.replace(persianNumbers[i], i).replace(arabicNumbers[i], i);
        return value;
    }
    function switchToPersian(value) {
        if (!value) {
            return;
        }
        for (var i = 0, charsLen = persianChar.length; i < charsLen; i++) {
            value = value.toString().replaceAll(englishChar[i].toUpperCase(), persianChar[i]);
            value = value.toString().replaceAll(englishChar[i].toLowerCase(), persianChar[i])
        }
        return value;
    }
    function switchToEnglish(value) {
        if (!value) {
            return;
        }
        value = fixPersianNumbers(value);
        for (var i = 0, charsLen = persianChar.length; i < charsLen; i++) {
            value = value.toString().replaceAll(persianChar[i], englishChar[i].toUpperCase());
            value = value.toString().replaceAll(persianChar[i], englishChar[i].toLowerCase());
        }
        return value;
    }

})();