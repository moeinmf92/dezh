(function () {

    'use strict';

    // Define directive
    angular
        .module('tableList', [])
        .directive('tableList', tableList)
        .directive('listItem', listItem)
        .directive('sortable', sortable);

    // Directive injections
    tableList.$inject = [
        '$parse',
        '$q',
        '$rootScope'
    ];
    listItem.$inject = [
        '$parse',
        '$timeout'
    ];
    sortable.$inject = [
        '$parse',
        '$timeout',
        '$rootScope',
        '$compile'
    ];

    // Directive object
    function tableList($parse, $q, $rootScope) {
        return {
            restrict: "A",
            scope: false,
            link: function (scope, element, attributes) {

                // Functions
                var Initialize = function () {
                    list = attributes.tableList;
                    handlers = $parse(attributes.tableListHandlers)(scope);
                    data = $parse(attributes.tableListData)(scope);
                    $rootScope.sort.field = data.sort_field ? data.sort_field : '';
                    $rootScope.sort.order = data.sort_order ? data.sort_order : '';
                    scope[list] = TableObject();

                };
                var GetList = function () {
                    scope[list].items.activeIndex = -1;
                    if (typeof handlers.get === 'function') {
                        var deferred = $q.defer();
                        data.sort_field = angular.copy($rootScope.sort.field);
                        data.sort_order = angular.copy($rootScope.sort.order);
                        handlers
                            .get(data)
                            .then(
                                function (response) {
                                    if (response){
                                        scope[list].items.count = response.total;
                                        scope[list].items.records = response.items;
                                        scope[list].items.current = Number(scope[list].page_num);
                                    }

                                    deferred.resolve();
                                },
                                function (response) {
                                    scope[list].items.count = 0;
                                    scope[list].items.records = [];
                                    deferred.reject();
                                }
                            )
                        return deferred.promise;
                    }
                };
                var ClickItem = function (index) {
                    var item = scope[list].items.records[index];
                    if (scope[list].items.activeIndex === -1) {
                        scope[list].items.activeIndex = index;
                        if (typeof handlers.select === 'function')
                            handlers.select(item);
                    } else if (scope[list].items.activeIndex !== index) {
                        scope[list].items.activeIndex = index;
                        if (typeof handlers.reselect === 'function')
                            handlers.reselect(item);
                    } else if (scope[list].items.activeIndex === index) {
                        scope[list].items.activeIndex = -1;
                        if (typeof handlers.unselect === 'function')
                            handlers.unselect(item);
                    }
                };
                var ChangePage = function (arg) {
                    scope[list].page_num = Number(scope[list].page_num);
                    if (arg)
                        scope[list].page_num += Number(arg);
                };
                var Untouched = function () {
                    scope[list].items.activeIndex = -1;
                    handlers.unselect(undefined);
                };
                var TableObject = function () {
                    return {
                        items: {
                            records: {},
                            count: 0,
                            current: 0,
                            activeClass: ' selected ',
                            blinkClass: ' blink ',
                            activeIndex: -1,
                            blinkIndex: -1
                        },
                        page_num: 1,
                        maxPage: 1,
                        params: data,
                        getList: GetList,
                        clickItem: ClickItem,
                        changePage: ChangePage,
                        untouched: Untouched
                    }
                };

                // Events
                scope.$on('GetList', function (event, data) {
                    GetList();
                });

                // Variables
                var list, handlers, data;
                $rootScope.sort = {};

                // Initialize
                Initialize();

                // Watchers
                scope.$watch(list + '.page_num', function (value) {
                    value = Number(value);
                    if (value !== undefined) {
                        var page_num = Number(scope[list].page_num);
                        var maxPage = angular.copy(Number(scope[list].maxPage));
                        if (page_num > maxPage)
                            return scope[list].page_num = maxPage;
                        else if (page_num < 1)
                            return scope[list].page_num = 1;
                        else if (page_num !== scope[list].items.current) {
                            data.page_num = Number(scope[list].page_num);
                            return GetList();
                        }
                    }
                });
                scope.$watch(list + '.items.count', function (value) {
                    if (value !== undefined) {
                        var maxPage = Math.ceil(scope[list].items.count / data.per_page);
                        scope[list].maxPage = maxPage ? maxPage : 1;
                    }
                });
                scope.$watch('sort.field', function (newValue,oldValue) {
                    // debugger
                    if (newValue !== undefined && newValue !== oldValue) {
                       GetList();
                    }
                });
                scope.$watch('sort.order', function (newValue,oldValue) {
                    // debugger
                    if (newValue !== undefined && newValue !== oldValue) {
                        GetList();
                    }
                });

            }
        }
    }

    function listItem($parse, $timeout) {
        return {
            restrict: "A",
            scope: false,
            link: function (scope, element, attributes) {

                // Functions
                var Initialize = function () {
                    name = attributes.listItem.split('list:')[1].split(',')[0].trim();
                    item = $parse(attributes.listItem)(scope);
                    list = item.list;
                };

                // Events
                element.on('click', function (event) {
                    event.stopPropagation();
                    list.clickItem(item.index);
                });

                // Variables
                var item, list, name;

                // Initialize
                Initialize();

                // Watchers
                scope.$watch(name + '.items.blinkIndex', function (value) {
                    if (value !== undefined) {
                        if (list.items.blinkIndex === item.index) {
                            element[0].className += list.items.blinkClass;
                            $timeout(function () {
                                element[0].className = element[0].className.replace(list.items.blinkClass, " ").replace(/^\s+|\s+$/g, "");
                            }, 999);
                        }
                    }
                });
                scope.$watch(name + '.items.activeIndex', function (value) {
                    if (value !== undefined) {
                        if (list.items.activeIndex !== item.index) {
                            if (element[0].className.indexOf(list.items.activeClass) !== -1)
                                element[0].className = element[0].className.replace(list.items.activeClass, " ").replace(/^\s+|\s+$/g, "");
                        }
                        else {
                            element[0].className += list.items.activeClass;
                        }
                    }
                });

            }
        }
    }


    function sortable($parse, $timeout, $rootScope, $compile) {
        return {
            restrict: "A",
            scope: false,
            link: function (scope, element, attributes) {

                // Functions
                var Initialize = function () {

                };

                // Events
                element.on('click', function (event) {
                    if (scope.sort.field !== scope.sortItem[attributes.sortable] )
                        scope.sort.order = 'desc';
                    else
                        scope.sort.order = scope.sort.order === 'desc' ? 'asc' : 'desc';
                    scope.sort.field = scope.sortItem[attributes.sortable] ;

                    if (!scope.$$phase)
                        scope.$apply();
                });

                // Variables
                if (!scope.sortItem)
                    scope.sortItem = {};
                scope.sortItem[attributes.sortable] = attributes.sortable;
                var descArrow = $compile(angular.element('<md-icon class="" font-icon-library="material-icons" ng-show="sort.field === sortItem.' + attributes.sortable + ' && sort.order === ' + "'desc'" + ' ">arrow_downward </md-icon>'))(scope);
                var ascArrow = $compile(angular.element('<md-icon class="" font-icon-library="material-icons" ng-show="sort.field === sortItem.' + attributes.sortable + ' && sort.order === ' + "'asc'" + ' ">arrow_upward </md-icon>'))(scope);



                element.append(descArrow);
                element.append(ascArrow);



                // Initialize
                Initialize();

                // Watchers


            }
        }
    }

    function sort(element, data) {

        // Functions
        var Initialize = function () {
            elements.push(element);
        };


        // Variables
        var elements = [];

        // Initialize
        Initialize();
    }

})();