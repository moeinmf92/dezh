(function () {

    'use strict';

    // Define directive
    angular
        .module('checkImage', [])
        .directive('checkImage', checkImage);

    // Directive injections
    checkImage.$inject = [
        '$http'
    ];

    // Directive object
    function checkImage($http) {
        return {
            restrict: "A",
            link: function (scope, element, attributes) {
                attributes.$observe('ngSrc', function (ngSrc) {
                    $http
                        .get(ngSrc)
                        .then(
                            function (response) {},
                            function (response) {
                                if(attributes.checkImage)
                                    return element.attr('src', attributes.checkImage);
                                return element.remove();
                            }
                        )
                });
            }
        }
    }

})();